package;

import BlockType.BlockColor;
import flixel.FlxSprite;
import flixel.math.FlxRect;
import flixel.addons.display.FlxSliceSprite;
import flash.geom.Rectangle;

class ClusterSprite extends FlxSliceSprite
{
    public function new()
    {
        super(AssetPaths.element_red_rectangle__png, new FlxRect(9, 9, 14, 45), 0, 0);
    }

    public function updateGraphic(x:Float, y:Float, width:Float, height:Float)
    {
        this.x = x;
        this.y = y;
        setSize(width, height);

        // Uncomment thos lines if using FlxSprite
        //setGraphicSize(Std.int(width), Std.int(height));
        //updateHitbox(); // This one fucks with the x/y when using FlxSliceSprite for some damn reason.
    }

    public function loadGraphicBasedOnBlock(type:BlockType, color:BlockColor)
    {
        switch (type)
        {
            case ClusterBlock(_):
                switch (color)
                {
                    case Red:
                        loadGraphic(AssetPaths.element_red_rectangle__png, false, Std.int(width), Std.int(height));
                    case Blue:
                        loadGraphic(AssetPaths.element_blue_rectangle__png, false, Std.int(width), Std.int(height));
                    case Green:
                        loadGraphic(AssetPaths.element_green_rectangle__png, false, Std.int(width), Std.int(height));
                    case Yellow:
                        loadGraphic(AssetPaths.element_yellow_rectangle__png, false, Std.int(width), Std.int(height));
                }

            case Strike(_):
                loadGraphic(AssetPaths.element_grey_rectangle__png, false, Std.int(width), Std.int(height));

            case Single, Breaker, Sprinkle(_):
        }
    }
}