package;

import flixel.FlxSprite;
import flixel.util.FlxColor;

class Background extends FlxSprite
{
    public function new(x:Float, y:Float)
    {
        super(x, y);
        makeGraphic(640, 320, FlxColor.BLACK);
    }
}