package;

import flixel.FlxObject;
import lime.math.Rectangle;
import Globals.BOARD_WIDTH;
import Globals.BOARD_HEIGHT;

@:allow(Rectangular)
class IRectangular
{
    public var x(get, set):Float;
    public var y(get, set):Float;
    public var width(get, never):Float;
    public var height(get, never):Float;

    var x_getter:Void->Float;
    var x_setter:Float->Float;
    var y_getter:Void->Float;
    var y_setter:Float->Float;
    var width_getter:Void->Float;
    var height_getter:Void->Float;

    function new(
        xGetter:Void->Float,
        xSetter:Float->Float,
        yGetter:Void->Float,
        ySetter:Float->Float,
        widthGetter:Void->Float,
        heightGetter:Void->Float)
    {
        x_getter = xGetter;
        x_setter = xSetter;
        y_getter = yGetter;
        y_setter = ySetter;
        width_getter = widthGetter;
        height_getter = heightGetter;
    }

    inline function get_x()
        return x_getter();
    inline function set_x(value:Float):Float
        return x_setter(value);
    inline function get_y()
        return y_getter();
    inline function set_y(value:Float):Float
        return y_setter(value);
    inline function get_width()
        return width_getter();
    inline function get_height()
        return height_getter();
}

@:forward(x, y, width, height)
abstract Rectangular(IRectangular)
{
    inline function new(
        xGetter:Void->Float,
        xSetter:Float->Float,
        yGetter:Void->Float,
        ySetter:Float->Float,
        widthGetter:Void->Float,
        heightGetter:Void->Float)
    {
        this = new IRectangular(
            xGetter,
            xSetter,
            yGetter,
            ySetter,
            widthGetter,
            heightGetter);
    }

    @:from
    public static function fromBlock(block:Block):Rectangular
    {
        return new Rectangular(
            ()->block.x,
            (v:Float)->return block.x = v,
            ()->block.y,
            (v:Float)->return block.y = v,
            ()->block.width,
            ()->block.height);
    }

    @:from
    public static function fromBlockPair(blockPair:BlockPair):Rectangular
    {
        return new Rectangular(
            ()->Math.min(blockPair.pivot_block.x, blockPair.rotating_block.x),
            function (v:Float) { trace("X cannot be set on BlockPair"); return v; },
            ()->Math.min(blockPair.pivot_block.y, blockPair.rotating_block.y),
            function (v:Float) { trace("Y cannot be set on BlockPair"); return v; },
            ()->blockPair.width,
            ()->blockPair.height);
    }

    @:from
    public static function fromRectangle(rect:Rectangle):Rectangular
    {
        return new Rectangular(
            ()->rect.x,
            (v:Float)->return rect.x = v,
            ()->rect.y,
            (v:Float)->return rect.y = v,
            ()->rect.width,
            ()->rect.height);
    }

    @:from
    public static function fromFlxObject(rect:FlxObject):Rectangular
    {
        return new Rectangular(
            ()->rect.x,
            (v:Float)->return rect.x = v,
            ()->rect.y,
            (v:Float)->return rect.y = v,
            ()->rect.width,
            ()->rect.height);
    }

    @:from
    public static function fromBoard(rect:Board):Rectangular
    {
        return new Rectangular(
            ()->rect.x,
            function (v:Float) { trace("X cannot be set on Board"); return v; },
            ()->rect.y,
            function (v:Float) { trace("Y cannot be set on Board"); return v; },
            ()->BOARD_WIDTH,
            ()->BOARD_HEIGHT);
    }

    public static function fromBlocks(rects:Array<Block>):Array<Rectangular>
    {
        var result = new Array<Rectangular>();
        for (rect in rects)
        {
            result.push(rect);
        }
        return result;
    }

    public static function fromBlockSet(rects:Set<Block>):Array<Rectangular>
    {
        var result = new Array<Rectangular>();
        for (rect in rects)
        {
            result.push(rect);
        }
        return result;
    }

    @:to
    public static function toRectangle(rect:IRectangular):Rectangle
    {
        return new Rectangle(rect.x, rect.y, rect.width, rect.height);
    }
}