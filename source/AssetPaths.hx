package;

@:build(flixel.system.FlxAssets.buildFileReferences("assets/images", true))
class AssetPaths {}

@:build(flixel.system.FlxAssets.buildFileReferences("assets/sounds", ["wav"]))
class Sounds {}