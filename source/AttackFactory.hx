package;

import flixel.math.FlxRandom;
import BasicTypes;
import Globals.*;
import BlockType;

using extensions.BasicTypesExtensions;
using extensions.ArrayExtensions;
using extensions.OptionExtensions;

class AttackFactory
{
    public static function createSprinkle(x:Float, y:Float):Block
    {
        var random = new FlxRandom();

        var color:BlockColor = Red;
        switch(random.int(0, SPAWNING_BLOCK_COLORS - 1))
        {
            case 0:
                color = Red;
            case 1:
                color = Blue;
            case 2:
                color = Green;
            case 3:
                color = Yellow;
        }

        return new Block(x, y, Sprinkle(2), color);
    }

    public static function createSprinkles(count:Int, startFromLeft:Bool, board:Board, sprites:Sprites)
    {
        var max_amount_sprinkles = board.getEmptySpacesFromTop();
        max_amount_sprinkles[3] -= 3; // 4th column can have max 10 sprinkles

        var row = 0;
        var column = -1;
        var created_sprinkles = 0;
        while (created_sprinkles < count)
        {
            column = if (startFromLeft)
                    (created_sprinkles % BOARD_X_CAPACITY)
                  else
                    (BOARD_X_CAPACITY - 1 - (created_sprinkles % BOARD_X_CAPACITY));
            row = (-1) - Math.floor(created_sprinkles / BOARD_X_CAPACITY);
            created_sprinkles++;

            if (max_amount_sprinkles[column] < (-row))
                continue;

            var sprinkle = AttackFactory.createSprinkle(column * BLOCK_WIDTH + board.x, row * BLOCK_HEIGHT + board.y);
            if (board.addFallingAttack(sprinkle))
                sprites.add(sprinkle);
        }
    }

    public static function createSprinklesFromStrike(strike:Block):Array<Block>
    {
        var sprinkles = [];

        switch (strike.type)
        {
            case Strike(var size, _):
                for (w in 0...size.width())
                {
                    for (h in 0...size.height())
                    {
                        var x = strike.x + w * BLOCK_WIDTH;
                        var y = strike.y + h * BLOCK_HEIGHT;

                        sprinkles.push(AttackFactory.createSprinkle(x, y));
                    }
                }
                return sprinkles;

            case Single, Breaker, ClusterBlock(_), Sprinkle(_):
                trace("WARNING: Incorrect block type given!");
                return [];
        }
    }

    public static function createStrike(x:Float, y:Float, size:Size):Block
    {
        return new Block(x, y, Strike(size, 1), Red);
    }

    public static function createStrikes(sizes:Queue<Size>, startFromLeft:Bool, initialAttackPosition:Int, board:Board, sprites:Sprites)
    {
        function correctSwordSizes(sizes:Queue<Size>):Queue<Size>
        {
            var corrected_sizes = new Queue<Size>();
            while (!sizes.empty())
            {
                var strike_size = sizes.pop();

                var blocks_in_strike = strike_size.width() * strike_size.height();
                if (blocks_in_strike < 4 || strike_size.width() <= 1 || strike_size.height() < 2)
                {
                    trace('WARNING: Swords of this size are not supported');
                    continue;
                }

                if (strike_size.width() == 2 && strike_size.height() == 2)
                    corrected_sizes.push(Size(1,4));
                else if (strike_size.width() == 3 && strike_size.height() == 2)
                    corrected_sizes.push(Size(2,3));
                else if (strike_size.width() == 3 && strike_size.height() == 3)
                    corrected_sizes.push(Size(2,4));
                else
                {
                    var blocks_width = Math.min(Math.min(strike_size.width(), strike_size.height()), 3);
                    var blocks_height = Math.min(Math.floor(blocks_in_strike / blocks_width), 13);
                    corrected_sizes.push(Size(Std.int(blocks_width), Std.int(blocks_height)));
                }
            }
            return corrected_sizes;
        }

        function getPositionSequences(strikeWidth:Int, startingNumber:Int, startFromLeft:Bool):Array<Array<Int>>
        {
            var positions = [];
            var ptr = 0;
            while (ptr < BOARD_X_CAPACITY)
            {
                var sequence = [for (i in ptr...(ptr + strikeWidth)) (i % BOARD_X_CAPACITY) + 1 ];

                if (sequence.first() <= sequence.last())
                    positions.push(sequence);

                ptr++;
            }

            if (positions.length >= startingNumber)
                positions.rotate(Std.int(Math.max(0, startingNumber - 1)));
            if (!startFromLeft)
                positions.reverse();

            return positions;
        }

        function getNonOverlappingSequences(sequences:Array<Array<Int>>, usedSequences:Array<Array<Int>>)
        {
            if (usedSequences.empty())
            {
                return sequences;
            }

            var non_overlapping_sequences = [];
            for (sequence in sequences)
            {
                var sequence_overlaps_with_used = sequence.anyOf(
                    function (position)
                    {
                        return usedSequences.anyOf(
                            function(usedSequence)
                            {
                                return usedSequence.anyOf((usedPosition) -> position == usedPosition);
                            });
                    });

                if (!sequence_overlaps_with_used)
                {
                    non_overlapping_sequences.push(sequence);
                }
            }
            return non_overlapping_sequences;
        }

        function getMostOptimalSequence(strikeHeight:Int, sequences:Array<Array<Int>>, emptySpaces:Array<Int>):Array<Int>
        {
            // Try find a sequence where the whole strike can fit.
            var most_optimal_sequence = sequences.find(function(sequence)
                {
                    return sequence.allOf((position) -> strikeHeight <= emptySpaces[position - 1]);
                });

            if (most_optimal_sequence.exists())
            {
                return most_optimal_sequence.value();
            }

            // Find the sequence where the smallest empty space is the largest
            return sequences.findMaxElement(function(sequence_left, sequence_right)
                {
                    // Find the smallest empty space for this sequence
                    var smallest_empty_space_left = sequence_left.findMaxElement((left, right) -> emptySpaces[left] < emptySpaces[right]);
                    var smallest_empty_space_right = sequence_right.findMaxElement((left, right) -> emptySpaces[left] < emptySpaces[right]);

                    return emptySpaces[smallest_empty_space_left] > emptySpaces[smallest_empty_space_right];
                });
        }

        var corrected_sizes = correctSwordSizes(sizes);
        var empty_spaces = board.getEmptySpacesFromTop();

        var used_sequences:Array<Array<Int>> = [];

        while (!corrected_sizes.empty())
        {
            var strike_size = corrected_sizes.pop();

            var sequences = getPositionSequences(strike_size.width(), initialAttackPosition, startFromLeft);
            sequences = getNonOverlappingSequences(sequences, used_sequences);

            var normal_sequences = [for (seq in sequences) if (!seq.contains(4)) seq];
            var sequences_with_four = [for (seq in sequences) if (seq.contains(4)) seq];

            var chosen_sequence = [];
            if (!normal_sequences.empty())
                chosen_sequence = getMostOptimalSequence(strike_size.height(), normal_sequences, empty_spaces);
            else if (!sequences_with_four.empty())
                chosen_sequence = getMostOptimalSequence(strike_size.height(), sequences_with_four, empty_spaces);
            else
                continue;

            used_sequences.push(chosen_sequence);

            var strike = createStrike(
                board.x + ((chosen_sequence.first() - 1) * BLOCK_WIDTH),
                (-1) * strike_size.height() * BLOCK_HEIGHT + board.y,
                strike_size);

            if (board.addFallingAttack(strike))
                sprites.add(strike);
        }

        //trace('Attacks: ${used_sequences} (Pos: ${initialAttackPosition}, Left: ${startFromLeft})');
    }
}