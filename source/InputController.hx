package;

import flixel.input.FlxInput.FlxInputState;
import flixel.FlxG;
import flixel.input.keyboard.FlxKey;
import InputState;

class InputController
{
    var input_states:Map<FlxKey, InputState>;

    public function new()
    {
        input_states = new Map<FlxKey, InputState>();
    }

    public static function isPressed(keyCode:FlxKey):Bool
    {
        return FlxG.keys.checkStatus(keyCode, FlxInputState.JUST_PRESSED) || FlxG.keys.checkStatus(keyCode, FlxInputState.PRESSED);
    }

    public function subscribeToKeyEvent(keyCode:FlxKey, timings:Range<Float>) 
    {
        if (input_states.exists(keyCode))
        {
            return;
        }

        input_states.set(keyCode, new InputState(timings));
    }

    public function unSubscribeToKeyEvent(keyCode:FlxKey) 
    {
        input_states.remove(keyCode);
    }

    public function isKeyPressed(keyCode:FlxKey, elapsed:Float):Bool
    {
        if (!input_states.exists(keyCode))
        {
            trace("WARNING: InputController is not subscribed to key " + keyCode.toString());
            return false;
        }

        var state = input_states[keyCode];

        if (FlxG.keys.checkStatus(keyCode, FlxInputState.JUST_PRESSED))
        {
            return true;
        }
        if (FlxG.keys.checkStatus(keyCode, FlxInputState.JUST_RELEASED))
        {
            state.elapsed = 0;
            state.timeout = state.timings.max;
            return false;
        }
        if (!FlxG.keys.checkStatus(keyCode, FlxInputState.PRESSED))
        {
            return false;
        }

        state.elapsed += elapsed;

        if (state.elapsed < state.timeout)
        {
            return false;
        }

        state.elapsed = 0;
        state.timeout = Math.max(state.timeout - state.timings.min, state.timings.min);

        return true;
    }
}