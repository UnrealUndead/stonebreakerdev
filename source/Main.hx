package;

import flixel.FlxGame;
import openfl.display.Sprite;
import TestState.TestState;
import NetworkTestState.NetworkTestState;
import PlayState.PlayState;
import Globals.GAME_WIDTH;
import Globals.GAME_HEIGHT;

class Main extends Sprite
{
	public function new()
	{
		super();
		addChild(new FlxGame(GAME_WIDTH, GAME_HEIGHT, PlayState, 1, 100, 60, true, false));
		//addChild(new FlxGame(GAME_WIDTH, GAME_HEIGHT, TestState, 1, 10, 10, true, false));
		//addChild(new FlxGame(GAME_WIDTH, GAME_HEIGHT, NetworkTestState, 1, 100, 60, true, false));
	}
}
