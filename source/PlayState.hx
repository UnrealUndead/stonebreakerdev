package;

import flixel.util.FlxAxes;
import AssetPaths.Sounds;
import flixel.util.FlxTimer;
import BasicTypes.GeneralDirection;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.FlxState;
import Globals.*;
import Factory;
import Set;

using extensions.ArrayExtensions;
using extensions.OptionExtensions;
using extensions.BasicTypesExtensions;

enum GameState
{
    Starting;
    Defending;
    Falling;
    Breaking;
    Attacking;
    Playing;
    Finished;
}

class PlayState extends FlxState
{
    var board:Board;
    var sprites:Sprites;

    var player_blocks:BlockPair;
    var next_player_blocks:BlockPair;

    var gameState:GameState = Playing;

    var consecutive_breaks_timer:FlxTimer;
    var before_break_timer:FlxTimer;

    var attack_calc:AttackCalculator;
    var attack_receiver:AttackReceiver;
    var defending_sprites:Sprites;
    var defending_board:Board;

    var movement_controller:MovementController;

    override public function create():Void
    {
        super.create();

        FlxG.camera.bgColor = 0xbbf2cfb3;
        FlxG.camera.antialiasing = true;
        FlxG.sound.muted = MUTE;
        FlxG.sound.volume = VOLUME;
        //FlxG.scaleMode = new flixel.system.scaleModes.PixelPerfectScaleMode.PixelPerfectScaleMode();

        consecutive_breaks_timer = new FlxTimer(FlxTimer.globalManager);
        consecutive_breaks_timer.loops = 1;
        consecutive_breaks_timer.finished = true;

        before_break_timer = new FlxTimer(FlxTimer.globalManager);
        before_break_timer.loops = 1;
        before_break_timer.finished = true;

        sprites = new Sprites();
        add(sprites.group);
        board = new Board(64, 16, sprites);

        movement_controller = new MovementController();

        attack_calc = new AttackCalculator();
        if (USE_DEFENDER_BOARD)
        {
            defending_sprites = new Sprites();
            add(defending_sprites.group);
            defending_board = new Board(272, 16, defending_sprites);
            attack_receiver = new AttackReceiver(defending_board, defending_sprites);
        }
        else
            attack_receiver = new AttackReceiver(board, sprites);

        /*for (x in 0...6)
        {
            for (y in 0...3)
            {
                var block = Factory.addNewBlock(Position(x,y), Single, Blue, board, sprites).value();
                board.addFallingBlock(block);
            }
        }*/

        /*Factory.addNewCluster(Position(0,7), Size(2, 2), Yellow, board, sprites);
        Factory.addNewCluster(Position(1,9), Size(2, 2), Blue, board, sprites);
        Factory.addNewCluster(Position(1,4), Size(2, 2), Blue, board, sprites);

        for (i in 11...13)
        {
            Factory.addNewBlock(Position(1,i), Single, Red, board, sprites);
        }

        for (i in 9...13)
        {
            Factory.addNewBlock(Position(0,i), Single, Yellow, board, sprites);
        }

        for (i in 6...9)
        {
            Factory.addNewBlock(Position(2,i), Single, Green, board, sprites);
        }*/

        /*Factory.addNewCluster(Position(0,11), Size(2, 2), Blue, board, sprites);
        Factory.addNewCluster(Position(0,6), Size(2, 2), Blue, board, sprites);

        for (i in 8...11)
        {
            Factory.addNewBlock(Position(0,i), Single, Red, board, sprites);
        }*/

        /*Factory.addNewCluster(Position(3,6), Size(2, 2), Blue, board, sprites);

        for (i in 3...5)
        {
            Factory.addNewBlock(Position(i,8), Single, Red, board, sprites);
            Factory.addNewBlock(Position(i,9), Single, Blue, board, sprites);
            Factory.addNewBlock(Position(i,10), Single, Red, board, sprites);
            Factory.addNewBlock(Position(i,11), Single, Blue, board, sprites);
            Factory.addNewBlock(Position(i,12), Single, Red, board, sprites);
        }

        for (i in 5...13)
        {
            Factory.addNewBlock(Position(5,i), Single, Red, board, sprites);
        }
        Factory.addNewBlock(Position(5,4), Breaker, Blue, board, sprites);*/

        next_player_blocks = Factory.createPlayerBlocks(16, 80, sprites);

        updateGameState();
    }

    override public function update(elapsed:Float):Void
    {
        //haxe.Timer.measure(function() {});
        super.update(elapsed);

        switch (gameState)
        {
            case Starting:
                player_blocks = next_player_blocks;
                Factory.movePlayerBlocksOnBoard(player_blocks, Position(3, -1), board);
                next_player_blocks = Factory.createPlayerBlocks(16, 80, sprites);

                attack_receiver.prepare();
                FlxG.camera.bgColor = attack_receiver.hasIncomingAttack() ? 0xbbff0000 : 0xbbf2cfb3;

                //attack_calc.add(new Set<Block>([for (_ in 0...1200) new Block(0,0,Single,Red)]));

                updateGameState();

            case Defending:
                attack_receiver.updateAttacks();
                updateGameState();

            case Falling:
                updateFallingBlocks(elapsed);

            case Breaking:
                updateBreakingBlocks();

            case Attacking:
                attack_receiver.addAttack(attack_calc.takeAttack());
                updateGameState();

            case Playing:
                movement_controller.updateBlockPairVelocity(player_blocks);
                movement_controller.updateBlockPairRotation(player_blocks, board, elapsed);
                movement_controller.updateBlockPairHorizontalMovement(player_blocks, board, elapsed);

                var pivot_will_touch_down = Collision.willTouchDown(player_blocks.pivot_block, board, elapsed);
                var rotating_will_touch_down = Collision.willTouchDown(player_blocks.rotating_block, board, elapsed);
                if (pivot_will_touch_down || rotating_will_touch_down)
                {
                    // Order is important
                    board.updateSprinkles();
                    detatchPlayerBlocks(pivot_will_touch_down, rotating_will_touch_down);
                    board.updateStrikes();

                    updateGameState();
                }

            case Finished:
        }

        if (USE_DEFENDER_BOARD)
            defending_board.updateFallingBlocks(elapsed);
    }

    function updateGameState()
    {
        if (board.hasFallingBlocks())
        {
            gameState = Falling;
            return;
        }
        else if (board.hasBreakingBlocks())
        {
            gameState = Breaking;
            return;
        }
        else if (!attack_calc.empty())
        {
            gameState = Attacking;
            return;
        }
        else if (attack_receiver.hasAttackForThisTurn())
        {
            gameState = Defending;
            return;
        }
        else if (player_blocks == null)
        {
            gameState = Starting;
            return;
        }
        else if (gameState.equals(Starting) && Collision.isTouchingDown(player_blocks, board))
        {
            gameState = Finished;
            return;
        }
        else
        {
            gameState = Playing;
        }
    }

    function detatchPlayerBlocks(willPivotBlockCollide:Bool, willRotatingBlockCollide:Bool):Void
    {
        sprites.remove(player_blocks);
        sprites.add(player_blocks.pivot_block);
        sprites.add(player_blocks.rotating_block);

        if (player_blocks.isVerticallyOriented() || (willPivotBlockCollide && willRotatingBlockCollide))
        {
            board.addFallingAndCollided([player_blocks.pivot_block, player_blocks.rotating_block], []);
        }
        else
        {
            if (willPivotBlockCollide)
                board.addFallingAndCollided([player_blocks.pivot_block], [player_blocks.rotating_block]);
            else
                board.addFallingAndCollided([player_blocks.rotating_block], [player_blocks.pivot_block]);
        }

        player_blocks = null;

        if (board.hasBreakingBlocks())
            before_break_timer.reset(FIRST_BEFORE_BREAK_TIME);
    }

    function updateFallingBlocks(elapsed:Float)
    {
        board.updateFallingBlocks(elapsed);

        if (board.hasFallingBlocks())
            return;

        if (board.hasBreakingBlocks())
            before_break_timer.reset(BEFORE_BREAK_TIME);

        updateGameState();
    }

    function updateBreakingBlocks()
    {
        if (!before_break_timer.finished)
            return;

        if (!consecutive_breaks_timer.finished)
            return;

        var broken_blocks = board.updateBreakingBlocks();

        if (!broken_blocks.empty())
        {
            attack_calc.add(broken_blocks);
            consecutive_breaks_timer.cancel();

            updateGameState();
        }
        else
        {
            //FlxG.camera.shake(0.002, 0.05, FlxAxes.XY);
            consecutive_breaks_timer.reset(BREAKING_TIME);
        }
    }
}
