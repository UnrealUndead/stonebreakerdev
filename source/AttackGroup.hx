package;

import BasicTypes;

class AttackGroup
{
    public var sprinkles:Int;
    public var strikes:Queue<Size>;

    public function new(sprinkles:Int, strikes:Queue<Size>)
    {
        this.sprinkles = sprinkles;
        this.strikes = strikes;
    }
}