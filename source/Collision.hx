package;

import BasicTypes;
import BasicTypes.GeneralDirection;
import haxe.EnumFlags;
import lime.math.Vector4;
import lime.math.Rectangle;
import flixel.FlxG;
import flixel.group.FlxGroup;
import Globals.*;

using extensions.RectangleExtensions;
using extensions.EnumFlagsExtensions;
using extensions.ArrayExtensions;

class Collision
{
    public static function isTouchingDown(rect:Rectangular, board:Board):Bool
    {
        var rectangular_obstacles = Rectangular.fromBlocks(board.getAllBlocks());
        var flags = new EnumFlags<GeneralDirection>();
        flags.set(GeneralDirection.Down);
        Collision.addWorldBounderies(rectangular_obstacles, flags, board);

        var colliding_objects = calculateCollision(rect, rectangular_obstacles, [Direction.Down(1)], []);
        return !colliding_objects.empty();
    }

    public static function willTouchDown(block:Block, board:Board, elapsed:Float):Bool
    {
        var offset = Math.ceil(block.velocity * elapsed) + 1;

        var rectangular_obstacles = Rectangular.fromBlocks(board.getAllBlocks());
        var flags = new EnumFlags<GeneralDirection>();
        flags.set(GeneralDirection.Down);
        Collision.addWorldBounderies(rectangular_obstacles, flags,board);

        var colliding_objects = calculateCollision(block, rectangular_obstacles, [Direction.Down(offset)], []);
        return determineCollisionDirection(block, colliding_objects, []).has(GeneralDirection.Down);
    }

    public static function willTouchDown2(block:Block, obstacles:Set<Block>, world:Rectangular, elapsed:Float):Bool
    {
        var offset = Math.ceil(block.velocity * elapsed) + 1;

        var rectangular_obstacles = Rectangular.fromBlocks(obstacles);
        var flags = new EnumFlags<GeneralDirection>();
        flags.set(GeneralDirection.Down);
        Collision.addWorldBounderies(rectangular_obstacles, flags, world);

        var colliding_objects = calculateCollision(block, rectangular_obstacles, [Direction.Down(offset)], []);
        return determineCollisionDirection(block, colliding_objects, []).has(GeneralDirection.Down);
    }

    public static function isRotatingBlockOverlapping(blockPair:BlockPair, board:Board):Bool
    {
        var rectangular_obstacles = Rectangular.fromBlocks(board.getAllBlocks());
        var flags = new EnumFlags<GeneralDirection>();
        flags.set(GeneralDirection.Down);
        flags.set(GeneralDirection.Left);
        flags.set(GeneralDirection.Right);
        Collision.addWorldBounderies(rectangular_obstacles, flags, board);
        addTopBoardBlocksBounderies(rectangular_obstacles, board);

        var colliding_objects = calculateCollision(blockPair.rotating_block, rectangular_obstacles,
            [],
            [Direction.Up(-ALLOWED_UPPER_COLLISION), Direction.Down(-ALLOWED_LOWER_COLLISION)]);
        return !colliding_objects.empty();
    }

    public static function getHorizontalPlayerCollision(rect:Rectangular, board:Board):EnumFlags<GeneralDirection>
    {
        var rectangular_obstacles = Rectangular.fromBlocks(board.getAllBlocks());
        var flags = new EnumFlags<GeneralDirection>();
        flags.set(GeneralDirection.Left);
        flags.set(GeneralDirection.Right);
        Collision.addWorldBounderies(rectangular_obstacles, flags, board);
        addTopBoardBlocksBounderies(rectangular_obstacles, board);

        var horizontal_colliding_objects = calculateCollision(rect, rectangular_obstacles,
            [Direction.Left(1), Direction.Right(1)],
            [Direction.Up(-ALLOWED_UPPER_COLLISION), Direction.Down(-ALLOWED_LOWER_COLLISION)]);
        return determineCollisionDirection(rect, [], horizontal_colliding_objects);
    }

    public static function getPlayerCollision(rect:Rectangular, board:Board, addWorldBounderies:Bool = true):EnumFlags<GeneralDirection>
    {
        var rectangular_obstacles = Rectangular.fromBlocks(board.getAllBlocks());
        if (addWorldBounderies)
        {
            var flags = new EnumFlags<GeneralDirection>();
            flags.set(GeneralDirection.Down);
            flags.set(GeneralDirection.Left);
            flags.set(GeneralDirection.Right);
            Collision.addWorldBounderies(rectangular_obstacles, flags, board);
            addTopBoardBlocksBounderies(rectangular_obstacles, board);
        }

        var vertical_colliding_objects = calculateCollision(rect, rectangular_obstacles,
            [Direction.Up(1), Direction.Down(1)],
            []);
        var horizontal_colliding_objects = calculateCollision(rect, rectangular_obstacles,
            [Direction.Left(1), Direction.Right(1)],
            [Direction.Up(-ALLOWED_UPPER_COLLISION), Direction.Down(-ALLOWED_LOWER_COLLISION)]);

        return determineCollisionDirection(rect, vertical_colliding_objects, horizontal_colliding_objects);
    }



    static function addTopBoardBlocksBounderies(obstacles:Array<Rectangular>,board:Board)
    {
        for (top_block in board.getTopBlocks())
            obstacles.push(new Rectangle(top_block.x, top_block.y - BLOCK_HEIGHT, top_block.width, top_block.height + BLOCK_HEIGHT));
    }

    static function addWorldBounderies(obstacles:Array<Rectangular>, flags:EnumFlags<GeneralDirection>, world:Rectangular)
    {
        if (flags.has(GeneralDirection.Up))
        {
            obstacles.push(new Rectangle(-5000, -10000 + world.y, 10000, 10000));
        }
        if (flags.has(GeneralDirection.Down))
        {
            obstacles.push(new Rectangle(-5000, world.y + world.height, 10000, 10000));
        }
        if (flags.has(GeneralDirection.Left))
        {
            obstacles.push(new Rectangle(-10000 + world.x, -5000, 10000, 10000));
        }
        if (flags.has(GeneralDirection.Right))
        {
            obstacles.push(new Rectangle(world.x + world.width, -5000, 10000, 10000));
        }
    }

    static function determineCollisionDirection(rect:Rectangular, verticallyCollidingObjects:Array<Rectangular>, horizontallyCollidingObjects:Array<Rectangular>):EnumFlags<GeneralDirection>
    {
        var collision = new EnumFlags<GeneralDirection>();

        for (obstacle in verticallyCollidingObjects)
        {
            if (rect.y >= obstacle.y)
            {
                collision.set(GeneralDirection.Up);
            }
            if (rect.y <= obstacle.y)
            {
                collision.set(GeneralDirection.Down);
            }
        }
        for (obstacle in horizontallyCollidingObjects)
        {
            if (rect.x >= obstacle.x)
            {
                collision.set(GeneralDirection.Left);
            }
            if (rect.x <= obstacle.x)
            {
                collision.set(GeneralDirection.Right);
            }
        }

        return collision;
    }

    static function toOffsetRectangle(rect:Rectangular, offset:Array<Direction>):Rectangle
    {
        var prepared_rect = rect.toRectangle();
        for (direction in offset)
        {
            switch (direction)
            {
                case Up(value):
                    prepared_rect.top -= value;
                case Down(value):
                    prepared_rect.bottom += value;
                case Left(value):
                    prepared_rect.left -= value;
                case Right(value):
                    prepared_rect.right += value;
                case _:
            }
        }
        return prepared_rect;
    }

    static function calculateCollision(rect:Rectangular, obstacles:Array<Rectangular>, rectOffset:Array<Direction>, obstaclesOffset:Array<Direction>):Array<Rectangular>
    {
        var adjusted_rect = toOffsetRectangle(rect, rectOffset);

        var colliding_obstacles = new Array<Rectangular>();
        for (i in 0...obstacles.length)
        {
            var obstacle = obstacles[i];
            if (adjusted_rect.intersects(toOffsetRectangle(obstacle, obstaclesOffset)))
            {
                colliding_obstacles.push(obstacle);
            }
        }

        return colliding_obstacles;
    }
}