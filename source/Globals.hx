package;

class Globals
{
    public static var GAME_WIDTH:Int = 464;
    public static var GAME_HEIGHT:Int = 848;

    public static var BLOCK_WIDTH:Int = 32;
    public static var BLOCK_HEIGHT:Int = 64;
    public static var SPRINKLES_OPACITY = 0.7;

    public static var BOARD_X_CAPACITY:Int = 6;
    public static var BOARD_Y_CAPACITY:Int = 13;
    public static var BOARD_WIDTH:Float = BOARD_X_CAPACITY * BLOCK_WIDTH;
    public static var BOARD_HEIGHT:Float = BOARD_Y_CAPACITY * BLOCK_HEIGHT;

    public static var NORMAL_VELOCITY:Float = 50;//50;
    public static var FAST_VELOCITY:Float = 1000;//1000
    public static var FALLING_VELOCITY:Float = 1400;//1800

    public static var BREAKER_SPAWN_PERCENTAGE:Int = 20;//25
    public static var SPAWNING_BLOCK_COLORS:Int = 4;//4
    public static var DISABLE_CLUSTERING:Bool = false;
    public static var USE_DEFENDER_BOARD:Bool = false;

    public static var ALLOWED_UPPER_COLLISION:Int = 16;//16
    public static var ALLOWED_LOWER_COLLISION:Int = 16;//16

    public static var BREAKING_TIME:Float = 120 / 1000;//60
    public static var BEFORE_BREAK_TIME:Float = 300 / 1000;//300
    public static var FIRST_BEFORE_BREAK_TIME:Float = 120 / 1000;//100

    public static var VOLUME:Float = 25 / 100;
    public static var MUTE:Bool = true;
}

