package;

import haxe.ds.Option;
import Globals.*;
import BasicTypes;
import Set;
import Block;

using extensions.BasicTypesExtensions;

class AdjacentBlocks
{
    public var topLeft(get, never):Option<Block>;
    public var topRight(get, never):Option<Block>;
    public var bottomLeft(get, never):Option<Block>;
    public var bottomRight(get, never):Option<Block>;

    public var top(get, never):Set<Block>;
    public var left(get, never):Set<Block>;
    public var bottom(get, never):Set<Block>;
    public var right(get, never):Set<Block>;

    var blocks:Map<Position, Block>;
    var size:Size;

    public function new(centerPosition:Position, centerSize:Size, blocks:Map<Position, Block>)
    {
        if (centerSize.width() < 1 || centerSize.height() < 1)
        {
            trace('WARNING: Adjacent size should be of minimum 1x1. Size passed ${centerSize.width()}x${centerSize.height()}');
            return;
        }

        this.size = centerSize.add(Size(2,2));
        this.blocks = new Map<Position, Block>();

        for (x in 0...size.width())
        {
            var adjacent_block = blocks.get(Position(centerPosition.x() - 1 + x,  centerPosition.y() - 1));
            if (adjacent_block != null)
                this.blocks.set(Position(x, 0), adjacent_block);

            adjacent_block = blocks.get(Position(centerPosition.x() - 1 + x, centerPosition.y() + centerSize.height()));
            if (adjacent_block != null)
                this.blocks.set(Position(x, size.height() - 1), adjacent_block);
        }

        for (y in 0...size.height())
        {
            var adjacent_block = blocks.get(Position(centerPosition.x() - 1,  centerPosition.y() - 1 + y));
            if (adjacent_block != null)
                this.blocks.set(Position(0, y), adjacent_block);

            adjacent_block = blocks.get(Position(centerPosition.x() + centerSize.width(), centerPosition.y() - 1 + y));
            if (adjacent_block != null)
                this.blocks.set(Position(size.width() - 1, y), adjacent_block);
        }
    }

    public function print()
    {
        var s = "\n\n";
        for (y in 0...size.height())
        {
            for (x in 0...size.width())
            {
                s += " ";

                if (!(x == 0 || y == 0 || x == (size.width() - 1) || y == (size.height() - 1)))
                {
                    s += "+";
                    continue;
                }

                if (!blocks.exists(Position(x,y)) || blocks.get(Position(x,y)) == null)
                {
                    s += ".";
                    continue;
                }

                var block = blocks.get(Position(x,y));
                switch (block.type)
                {
                    case Single:
                        s += "S";
                    case ClusterBlock(_):
                        s += "C";
                    case Breaker:
                        s += "B";
                    case Sprinkle(_):
                        s += "P";
                    case Strike(_):
                        s += "T";
                }
            }
            s += "\n";
        }
        trace(s);
    }

    public function getDirectlyAdjacent():Set<Block>
    {
        return top + bottom + left + right;
    }

    function get_topLeft():Option<Block>
    {
        var block = blocks.get(Position(0,0));
        if (block == null)
            return None;
        return Some(block);
    }

    function get_topRight():Option<Block>
    {
        var block = blocks.get(Position(size.width() - 1, 0));
        if (block == null)
            return None;
        return Some(block);
    }

    function get_bottomLeft():Option<Block>
    {
        var block = blocks.get(Position(0, size.height() - 1));
        if (block == null)
            return None;
        return Some(block);
    }

    function get_bottomRight():Option<Block>
    {
        var block = blocks.get(Position(size.width() - 1, size.height() - 1));
        if (block == null)
            return None;
        return Some(block);
    }

    function get_top():Set<Block>
    {
        var blocks = new Set<Block>();
        for (w in 1...(size.width() - 1))
        {
            var block = this.blocks.get(Position(w, 0));
            if (block != null)
                blocks.add(block);
        }
        return blocks;
    }

    function get_bottom():Set<Block>
    {
        var blocks = new Set<Block>();
        for (w in 1...(size.width() - 1))
        {
            var block = this.blocks.get(Position(w, size.height() - 1));
            if (block != null)
                blocks.add(block);
        }
        return blocks;
    }

    function get_left():Set<Block>
    {
        var blocks = new Set<Block>();
        for (h in 1...(size.height() - 1))
        {
            var block = this.blocks.get(Position(0, h));
            if (block != null)
                blocks.add(block);
        }
        return blocks;
    }

    function get_right():Set<Block>
    {
        var blocks = new Set<Block>();
        for (h in 1...(size.height() - 1))
        {
            var block = this.blocks.get(Position(size.width() - 1, h));
            if (block != null)
                blocks.add(block);
        }
        return blocks;
    }
}