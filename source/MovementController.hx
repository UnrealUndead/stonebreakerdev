package;

import haxe.EnumFlags;
import BasicTypes.GeneralDirection;
import flixel.FlxG;
import flixel.FlxG.console;
import BlockPair.RotationDirection;
import haxe.ds.Option;
import haxe.ds.Vector;
import BlockPair.RotatingState;
import InputState.Range;
import flixel.group.FlxGroup;
import Globals.*;
import Keys.*;

class MovementController
{
    static var MIN_TIMEOUT:Float = 0.15;
    static var MAX_TIMEOUT:Float = 0.15;
    static var DELTA_TIMEOUT:Float = 0.01;

    static var ROTATION_TIMEOUT:Float = Math.POSITIVE_INFINITY;

    var input_controller:InputController;

    public function new()
    {
        FlxG.console.registerFunction("setTimeout", function(value:Int)
        {
            MovementController.MAX_TIMEOUT = value;
        });

        input_controller = new InputController();
        input_controller.subscribeToKeyEvent(Keys.MOVE_LEFT, new Range<Float>(MIN_TIMEOUT, DELTA_TIMEOUT, MAX_TIMEOUT));
        input_controller.subscribeToKeyEvent(Keys.MOVE_RIGHT, new Range<Float>(MIN_TIMEOUT, DELTA_TIMEOUT, MAX_TIMEOUT));
        input_controller.subscribeToKeyEvent(Keys.ROTATE_CLOCKWISE, new Range<Float>(ROTATION_TIMEOUT, 0, ROTATION_TIMEOUT));
        input_controller.subscribeToKeyEvent(Keys.ROTATE_COUNTER_CLOCKWISE, new Range<Float>(ROTATION_TIMEOUT, 0, ROTATION_TIMEOUT));
    }

    function getRotationDirection(elapsed:Float):Option<RotationDirection>
    {
        if (input_controller.isKeyPressed(Keys.ROTATE_CLOCKWISE, elapsed))
        {
            return Some(RotationDirection.CLOCKWISE);
        }
        else if (input_controller.isKeyPressed(Keys.ROTATE_COUNTER_CLOCKWISE, elapsed))
        {
            return Some(RotationDirection.COUNTER_CLOCKWISE);
        }
        return None;
    }

    public function updateBlockPairRotation(blockPair:BlockPair, board:Board, elapsed:Float):Void
    {
        var result = getRotationDirection(elapsed);
        switch (result)
        {
            case Some(direction):
                rotateBlockPair(blockPair, board, direction, elapsed);
                return;
            case _:
                return;
        }
    }

    public static function rotateBlockPair(blockPair:BlockPair, board:Board, rotationDirection:RotationDirection, elapsed:Float):Void
    {
        var pivot_collision = Collision.getHorizontalPlayerCollision(blockPair, board);

        while (true)
        {
            blockPair.rotate(rotationDirection);

            if (!Collision.isRotatingBlockOverlapping(blockPair, board))
            {
                break;
            }

            if (blockPair.state == RotatingState.LEFT && pivot_collision.has(GeneralDirection.Left) && !pivot_collision.has(GeneralDirection.Right))
            {
                blockPair.x += blockPair.pivot_block.width;
                break;
            }
            else if (blockPair.state == RotatingState.RIGHT && pivot_collision.has(GeneralDirection.Right) && !pivot_collision.has(GeneralDirection.Left))
            {
                blockPair.x -= blockPair.pivot_block.width;
                break;
            }
        }
    }

	public function updateBlockPairVelocity(blockPair:BlockPair)
    {
        if (InputController.isPressed(Keys.MOVE_DOWN))
        {
            blockPair.velocity = FAST_VELOCITY;
        }
        else
        {
            blockPair.velocity = NORMAL_VELOCITY;
        }

        if (InputController.isPressed(Keys.ROTATE_CLOCKWISE) || InputController.isPressed(Keys.ROTATE_COUNTER_CLOCKWISE))
        {
            blockPair.velocity *= 0.7;
        }
    }

    public function updateBlockPairHorizontalMovement(blockPair:BlockPair, board:Board, elapsed:Float)
	{
        var is_left_pressed = input_controller.isKeyPressed(Keys.MOVE_LEFT, elapsed);
        var is_right_pressed = input_controller.isKeyPressed(Keys.MOVE_RIGHT, elapsed);

        var collision = Collision.getHorizontalPlayerCollision(blockPair, board);

        if (!collision.has(GeneralDirection.Left) && is_left_pressed)
        {
            blockPair.x -= blockPair.pivot_block.width;
        }
        else if (!collision.has(GeneralDirection.Right) && is_right_pressed)
        {
            blockPair.x += blockPair.pivot_block.width;
        }
    }
}