package;

import BasicTypes.Size;

enum BlockColor
{
    Red;
    Green;
    Blue;
    Yellow;
}

enum BlockState
{
    Static;
    Falling;
    Breaking;
    Broken;
    Custom;
}

enum BlockType
{
    Single;
    Breaker;
    ClusterBlock(size:Size);
    Sprinkle(turns:Int);
    Strike(size:Size, turns:Int);
}