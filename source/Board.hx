package;

import flixel.FlxSprite;
import haxe.ds.Either;
import AssetPaths.Sounds;
import flixel.FlxG;
import BlockType;
import haxe.ds.Option;
import BasicTypes;
import Globals.*;
import AttackFactory;

using extensions.ArrayExtensions;
using extensions.BasicTypesExtensions;
using extensions.MapExtensions;
using extensions.OptionExtensions;

class Board
{
    public var x(default, null):Float;
    public var y(default, null):Float;

    var grid:Grid;
    var sprites:Sprites;

    var blocks_map:Map<Position,Block>;
    var falling:Set<Block>;
    var fallen_blocks:Array<Block>;
    var breaking_blocks:BreakingBlocks;
    var broken_blocks:Set<Block>;
    var sprinkles:Set<Block>;
    var strikes:Set<Block>;

    var cached_blocks:CachedValue<Set<Block>>;
    var cached_breakers:CachedValue<Set<Block>>;
    var cached_clusters:CachedValue<Set<Block>>;

    public function new(x:Float, y:Float, sprites:Sprites)
    {
        this.x = x;
        this.y = y;
        grid = new Grid(Position(Std.int(x), Std.int(y) - BLOCK_HEIGHT * BOARD_Y_CAPACITY),
                        Size(BLOCK_WIDTH, BLOCK_HEIGHT),
                        Size(BOARD_X_CAPACITY, 2 * BOARD_Y_CAPACITY));
        blocks_map = new Map<Position,Block>();
        falling = new Set<Block>();
        fallen_blocks = new Array<Block>();
        breaking_blocks = new BreakingBlocks();
        broken_blocks = new Set<Block>();
        sprinkles = new Set<Block>();
        strikes = new Set<Block>();
        this.sprites = sprites;

        cached_blocks = new CachedValue<Set<Block>>(function ()
            {
                return new Set<Block>(blocks_map.values());
            });
        cached_breakers = new CachedValue<Set<Block>>(function ()
            {
                return new Set<Block>(cached_blocks.get().toArray().filter((block) -> block.type.equals(Breaker)));
            });
        cached_clusters = new CachedValue<Set<Block>>(function ()
            {
                return new Set<Block>(cached_blocks.get().toArray().filter((block) -> block.isCluster()));
            });
    }

    function addSingle(block:Block):Bool
    {
        var block_position = grid.getPosition(block, true);

        if (!isValidYPosition(block_position, block.type))
            return false;

        if (!isPositionAvailable(block_position, block.type))
        {
            trace('WARNING: Block position already occupied. Cannot add block on position x:${block_position.x()} y:${block_position.y()}');
            return false;
        }

        block.setState(Static);
        grid.snapToGrid(block);

        cached_blocks.markDirty();

        switch (block.type)
        {
            case Single:
                this.blocks_map.set(block_position, block);

            case Breaker:
                cached_breakers.markDirty();
                this.blocks_map.set(block_position, block);

            case Sprinkle(_):
                this.blocks_map.set(block_position, block);
                sprinkles.add(block);

            case ClusterBlock(var size):
                cached_clusters.markDirty();
                for (w in 0...size.width())
                    for (h in 0...size.height())
                        this.blocks_map.set(Position(block_position.x() + w, block_position.y() + h), block);

            case Strike(var size, _):
                strikes.add(block);
                for (w in 0...size.width())
                    for (h in 0...size.height())
                        this.blocks_map.set(Position(block_position.x() + w, block_position.y() + h), block);

        }
        return true;
    }

    public function add(blocks:Array<Block>)
    {
        for (block in blocks)
        {
            if (!addSingle(block))
            {
                remove(block); // Not needed for current flows, but for safety
                sprites.remove(block);
                block.sprite.destroy();
            }
        }
    }

    public function remove(block:Block)
    {
        var positions_to_remove = [];
        for (position => member in blocks_map)
        {
            if (member == block)
            {
                positions_to_remove.push(position);
                switch (block.type)
                {
                    case Sprinkle(_):
                        sprinkles.remove(block);
                    case Breaker:
                        cached_breakers.markDirty();
                    case ClusterBlock(_):
                        cached_clusters.markDirty();
                    case Strike(_, _):
                        strikes.remove(block);
                    case Single:
                }
            }
        }

        if (positions_to_remove.empty())
            return;

        cached_blocks.markDirty();

        for (position in positions_to_remove)
            blocks_map.remove(position);

        block.setState(Custom);
    }

    public function addFallingAndCollided(collidedBlocks:Array<Block>, fallingBlocks:Array<Block>)
    {
        add(collidedBlocks);
        if (!fallingBlocks.empty())
            for (block in collidedBlocks)
                fallen_blocks.push(block);

        for (block in fallingBlocks)
        {
            grid.snapToGrid(block);
            addFallingBlock(block);
        }

        if (falling.empty())
        {
            ClusteringLogic.updateClustersBasedOnNewBlocks(collidedBlocks, this, sprites);
            breaking_blocks.updateBreakers(this);
        }

        FlxG.sound.play(Sounds.domino_place__wav);
    }

    public function addFallingBlock(block:Block)
    {
        if (!block.state.equals(Static) && !block.state.equals(Custom))
            return;

        var adjacent = getAdjacentBlocks(block);
        if (!adjacent.bottom.toArray().anyOf((block) -> switch (block.state) { case Static: return true; case _: return false; }))
        {
            remove(block);
            block.setState(Falling);
            falling.add(block);

            for (top_adjacent in adjacent.top)
                addFallingBlock(top_adjacent);
        }
    }

    public function addFallingAttack(block:Block):Bool
    {
        switch (block.type)
        {
            case Sprinkle(_):
            case Strike(_, _):

            case _: return false;
        }

        block.setState(Falling);
        falling.add(block);

        switch (block.type)
        {
            case Sprinkle(_):
                sprinkles.add(block);
            case Strike(_, _):
                strikes.add(block);
            case _:
        }
        return true;
    }

    public function updateFallingBlocks(elapsed:Float)
    {
        var blocks = getAllBlocks();
        var blocks_touched_down = [];
        var blocks_touched_down_count = 0;
        do
        {
            blocks_touched_down_count = blocks_touched_down.length;
            for (block in falling)
            {
                if (Collision.willTouchDown2(block, blocks, this, elapsed))
                {
                    blocks_touched_down.push(block);
                    fallen_blocks.push(block);
                    falling.remove(block);
                    blocks.add(block);
                    break;
                }
            }
        } while (blocks_touched_down_count != blocks_touched_down.length);

        add(blocks_touched_down);

        if (falling.empty())
        {
            ClusteringLogic.updateClustersBasedOnNewBlocks(fallen_blocks, this, sprites);
            fallen_blocks = [];
            breaking_blocks.updateBreakers(this);
        }
    }

    public function updateBreakingBlocks():Set<Block>
    {
        if (breaking_blocks.empty())
            return new Set<Block>();

        var dequed_broken_blocks = breaking_blocks.dequeue().value();
        for (broken_block in dequed_broken_blocks)
        {
            sprites.remove(broken_block);
            broken_block.setState(Broken);
            broken_blocks.add(broken_block);
        }
        FlxG.sound.play(Sounds.glass_break__wav); // TODO handle cluster sound.

        if (breaking_blocks.empty())
        {
            for (broken_block in broken_blocks)
            {
                var adjacent = getAdjacentBlocks(broken_block);
                for (top_block in adjacent.top)
                    addFallingBlock(top_block);

                remove(broken_block);
                broken_block.sprite.destroy();
            }

            var result = broken_blocks.copy();
            broken_blocks = new Set<Block>();//.clear();
            return result;
        }
        return new Set<Block>();
    }

    public function updateSprinkles()
    {
        if (sprinkles.empty())
            return;

        var transformed_blocks = [];
        for (block in sprinkles)
        {
            switch (block.type)
            {
                case Sprinkle(turns):
                    if (turns == 1)
                    {
                        sprites.remove(block);
                        transformed_blocks.push(block);
                    }
                    var new_turns = block.decrementTurns();
                    if (new_turns == 1)
                    {
                        block.overlay_sprite.value().alpha = SPRINKLES_OPACITY;
                    }

                case Single, Breaker, ClusterBlock(_), Strike(_):
                    trace("WARNING: Incorrect block type in sprinkle list!");
            }
        }

        for (block in transformed_blocks)
            sprinkles.remove(block);

        if (!transformed_blocks.empty())
        {
            ClusteringLogic.updateClustersBasedOnNewBlocks(transformed_blocks, this, sprites);
            breaking_blocks.updateBreakers(this);
        }
    }

    public function updateStrikes()
    {
        if (strikes.empty())
            return;

        var transformed_sprinkles = [];
        var potentially_falling_blocks = new Set<Block>();

        for (strike in strikes)
        {
            switch (strike.type)
            {
                case Strike(var size, _):
                    var new_turns = strike.decrementTurns();
                    if (new_turns == 0)
                    {
                        var adjacent = getAdjacentBlocks(strike);
                        potentially_falling_blocks += adjacent.top;

                        transformed_sprinkles = transformed_sprinkles.concat(AttackFactory.createSprinklesFromStrike(strike));

                        sprites.remove(strike);
                        remove(strike);
                    }

                case Single, Breaker, ClusterBlock(_), Sprinkle(_):
                    trace("WARNING: Incorrect block type in strikes list!");
            }
        }

        strikes.clear(); // TODO: questionable

        for (block in potentially_falling_blocks)
        {
            addFallingBlock(block);
        }

        for (sprinkle in transformed_sprinkles)
        {
            if (addFallingAttack(sprinkle))
            {
                sprinkles.add(sprinkle);
                sprites.add(sprinkle);
            }
        }
    }

    public function getPosition(block:Block):Position
    {
        if (!getAllBlocks().contains(block))
            return Invalid;

        return grid.getPosition(block);
    }

    public function getBlock(position:Position):Option<Block>
    {
        switch (position)
        {
            case Position(_, _):
                var block = blocks_map.get(position);
                if (block != null)
                    return Some(block);
            case _:
        }
        return None;
    }

    public function getAdjacentBlocks(block:Block)
    {
        var block_size = Size(1,1);
        switch (block.type)
        {
            case ClusterBlock(var size), Strike(var size, _):
                block_size = size;
            case Single, Breaker, Sprinkle(_):
        }

        return new AdjacentBlocks(grid.getPosition(block), block_size, this.blocks_map);
    }

    public function getTopBlocks():Set<Block>
    {
        var result = new Set<Block>();
        for (position => block in blocks_map)
            if (position.y() == 0)
                result.add(block);
        return result;
    }

    public function getEmptySpacesFromTop():Array<Int>
    {
        var smallest_y = [for (_ in 0...BOARD_X_CAPACITY) BOARD_Y_CAPACITY];
        for (position in blocks_map.keys())
        {
            var y_position = Math.max(0, position.y() - BOARD_Y_CAPACITY); // Swords landing above board have negative positions
            if (y_position < smallest_y[position.x()])
                smallest_y[position.x()] = Std.int(y_position);
        }
        return smallest_y;
    }

    function isValidYPosition(position:Position, blockType:BlockType):Bool
    {
        switch (blockType)
        {
            case Strike(_, _): // Strike can always be added

            case _:
                if (position.y() < BOARD_Y_CAPACITY)
                    return false;
        }
        return true;
    }

    function isPositionAvailable(position:Position, blockType:BlockType):Bool
    {
        switch (blockType)
        {
            case Single, Breaker, Sprinkle(_):
                return !this.blocks_map.exists(position);

            case ClusterBlock(var size), Strike(var size, _):
                for (w in 0...size.width())
                {
                    for (h in 0...size.height())
                    {
                        if (this.blocks_map.exists(Position(position.x() + w, position.y() + h)))
                            return false;
                    }
                }
                return true;
        }
    }

    public function getAllBlocks():Set<Block>
    {
        return cached_blocks.get();
    }

    public function getBreakers():Set<Block>
    {
        return cached_breakers.get();
    }

    public function getClusters():Set<Block>
    {
        return cached_clusters.get();
    }

    public inline function hasFallingBlocks()
    {
        return !falling.empty();
    }

    public inline function hasBreakingBlocks()
    {
        return !breaking_blocks.empty();
    }

    public inline function hasSprinkles()
    {
        return !sprinkles.empty();
    }

    public function print()
    {
        var s = "\n\n";
        for (y in 0...grid.size.height())
        {
            if (y == BOARD_Y_CAPACITY)
                s += "--------------\n";
            s += y + ":";
            if (y < 10)
                s+= " ";
            for (x in 0...grid.size.width())
            {
                s += " ";
                if (!blocks_map.exists(Position(x,y)) || blocks_map.get(Position(x,y)) == null)
                {
                    s += ".";
                    continue;
                }

                var block = blocks_map.get(Position(x,y));
                switch (block.type)
                {
                    case Single:
                        s += "S";
                    case ClusterBlock(_):
                        s += "C";
                    case Breaker:
                        s += "B";
                    case Sprinkle(_):
                        s += "P";
                    case Strike(_):
                        s += "T";
                }
            }
            s += "\n";
        }
        trace(s);
    }
}
