package;

import BlockType;
import lime.math.Rectangle;
import BasicTypes.Size;
import haxe.ds.Option;
import Globals.*;

using extensions.ArrayExtensions;
using extensions.OptionExtensions;
using extensions.RectangleExtensions;
using extensions.BasicTypesExtensions;
using haxe.EnumTools;

class ClusteringLogic
{
    public static function updateClustersBasedOnNewBlocks(blocks:Array<Block>, board:Board, sprites:Sprites)
    {
        if (DISABLE_CLUSTERING)
            return

        if (blocks.empty())
            return;

        var single_blocks = new Set<Block>();
        var cluster_blocks = new Set<Block>();

        for (block in blocks)
        {
            if (board.getPosition(block).equals(Invalid))
                continue;

            if (block.type.equals(Single))
                single_blocks.add(block);
            else if (block.isCluster())
                cluster_blocks.add(block);
        }

        // Expand clusters based on new blocks
        var remaining_single_blocks = single_blocks.copy();
        for (block in single_blocks)
        {
            var adjacent = board.getAdjacentBlocks(block).getDirectlyAdjacent();
            for (adjacent_cluster in adjacent)
            {
                if (!adjacent_cluster.isCluster())
                    continue;

                switch (ClusteringLogic.tryExpandingCluster(adjacent_cluster, board, sprites))
                {
                    case Some(expanded_cluster) if (expanded_cluster != adjacent_cluster):
                        cluster_blocks.add(expanded_cluster);
                        remaining_single_blocks.remove(block);
                    case _:
                }
            }
        }

        // Create new clusters based on new blocks
        var new_clusters = new Set<Block>();
        for (block in remaining_single_blocks)
        {
            if (board.getPosition(block).equals(Invalid))
                continue;

            switch (ClusteringLogic.tryFormingSmallestCluster(block, board, sprites))
            {
                case Some(cluster):
                    new_clusters.add(cluster);
                    cluster_blocks.add(cluster);
                case None:
            }
        }

        // Expand those new clusters
        for (cluster in new_clusters)
        {
            switch (ClusteringLogic.tryExpandingCluster(cluster, board, sprites))
            {
                case Some(expanded_cluster) if (expanded_cluster != cluster):
                    cluster_blocks.remove(cluster);
                    cluster_blocks.add(expanded_cluster);
                case _:
            }
        }

        ClusteringLogic.tryMergingClusters(cluster_blocks, board, sprites);
    }

    static function tryFormingSmallestCluster(block:Block, board:Board, sprites:Sprites):Option<Block>
    {
        if (!block.type.equals(Single))
            return None;

        var surround = board.getAdjacentBlocks(block);

        var potential_clusters = [];
        if (surround.bottomRight.exists())
            potential_clusters.push(surround.bottom + surround.right + new Set<Block>([surround.bottomRight.value(), block]));
        if (surround.bottomLeft.exists())
            potential_clusters.push(surround.bottom + surround.left + new Set<Block>([surround.bottomLeft.value(), block]));
        if (surround.topRight.exists())
            potential_clusters.push(surround.top + surround.right + new Set<Block>([surround.topRight.value(), block]));
        if (surround.topLeft.exists())
            potential_clusters.push(surround.top + surround.left + new Set<Block>([surround.topLeft.value(), block]));

        for (blocks in potential_clusters)
        {
            if (blocks.length < 4)
                continue;

            switch (Factory.createClusterFromBlocks(blocks, board, sprites))
            {
                case Some(cluster):
                    return Some(cluster);
                case None:
            }
        }
        return None;
    }

    static function tryExpandingCluster(cluster:Block, board:Board, sprites:Sprites):Option<Block>
    {
        var surround = board.getAdjacentBlocks(cluster);

        var potential_expansions_map =
        [
            [surround.right.toArray(), surround.left.toArray()] => cluster.getClusterSize().height(),
            [surround.bottom.toArray(), surround.top.toArray()] => cluster.getClusterSize().width()
        ];

        for (potential_expansions => cluster_size in potential_expansions_map)
        {
            for (surrounding_blocks in potential_expansions)
            {
                if (surrounding_blocks.length < cluster_size)
                    continue;

                switch (Factory.createExpandedCluster(cluster, surrounding_blocks, board, sprites))
                {
                    case Some(expanded_cluster):
                        return tryExpandingCluster(expanded_cluster, board, sprites);
                    case None:
                }
            }
        }
        return Some(cluster);
    }

    static function tryMergingClusters(changedClusters:Set<Block>, board:Board, sprites:Sprites)
    {
        for (changed_cluster in changedClusters)
        {
            if (!changed_cluster.isCluster())
                continue;

            for (existing_cluster in board.getClusters())
            {
                switch(Factory.createMergedCluster(changed_cluster, existing_cluster, board, sprites))
                {
                    case Some(merged_cluster):
                        changedClusters.remove(changed_cluster);
                        changedClusters.remove(existing_cluster);
                        changedClusters.add(merged_cluster);
                        ClusteringLogic.tryMergingClusters(changedClusters, board, sprites);
                        return;

                    case None:
                }
            }
        }
    }
}