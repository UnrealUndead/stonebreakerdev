package;

import flixel.math.FlxRandom;
import BasicTypes;
import Globals.*;
import BlockType;
import haxe.ds.Option;
import lime.math.Rectangle;

using extensions.ArrayExtensions;
using extensions.RectangleExtensions;
using extensions.BasicTypesExtensions;

class Factory
{
    public static function createRandomBlock(x:Float, y:Float):Block
    {
        var random = new FlxRandom();

        var type:BlockType = Single;
        switch(random.int(0, 99))
        {
            case i if (i < BREAKER_SPAWN_PERCENTAGE):
                type = Breaker;
            case _:
                type = Single;
        }

        var color:BlockColor = Red;
        switch(random.int(0, SPAWNING_BLOCK_COLORS - 1))
        {
            case 0:
                color = Red;
            case 1:
                color = Blue;
            case 2:
                color = Green;
            case 3:
                color = Yellow;
        }

        return new Block(x, y, type, color);
    }

    public static function createPlayerBlocks(x:Float, y:Float, sprites:Sprites):BlockPair
    {
        var pivot_block = Factory.createRandomBlock(x, y);
        var rotating_block = Factory.createRandomBlock(x, y - BLOCK_HEIGHT);
        var block_pair = new BlockPair(pivot_block, rotating_block);
        sprites.add(block_pair);

        return block_pair;
    }

    public static function createPlayerBlocksOnBoard(position:Position, board:Board, sprites:Sprites):BlockPair
    {
        var block_pair = createPlayerBlocks(0, 0, sprites);
        movePlayerBlocksOnBoard(block_pair, position, board);
        return block_pair;
    }

    public static function movePlayerBlocksOnBoard(blocks:BlockPair, position:Position, board:Board)
    {
        blocks.x = position.x() * BLOCK_WIDTH + board.x;
        blocks.y = position.y() * BLOCK_HEIGHT + board.y;
    }

    public static function addNewBlock(position:Position, type:BlockType, color:BlockColor, board:Board, sprites:Sprites):Option<Block>
    {
        switch (position)
        {
            case Position(x, y):
                var block = new Block(x * BLOCK_WIDTH + board.x, y * BLOCK_HEIGHT + board.y, type, color);
                board.add([block]);
                sprites.add(block);
                return Some(block);
            case _:
        }
        return None;
    }

    public static function addNewCluster(position:Position, size:Size, color:BlockColor, board:Board, sprites:Sprites):Option<Block>
    {
        switch (position)
        {
            case Position(x_pos, y_pos):
                switch (size)
                {
                    case Size(_, _):
                        var x = x_pos * BLOCK_WIDTH + board.x;
                        var y = y_pos * BLOCK_HEIGHT + board.y;

                        var cluster = Factory.createCluster(x, y, size, color);
                        board.add([cluster]);
                        sprites.add(cluster);
                        return Some(cluster);
                    case _:
                }
            case _:
        }
        return None;
    }

    public static function createCluster(x:Float, y:Float, size:Size, color:BlockColor):Block
    {
        return new Block(x, y, ClusterBlock(size), color);
    }

    public static function createClusterFromBlocks(blocks:Array<Block>, board:Board, sprites:Sprites):Option<Block>
    {
        if (!allBlocksAreSingleAndSameColor(blocks))
            return None;

        var rect = getContainingRectangle(blocks);
        var width = Math.round(rect.width / BLOCK_WIDTH);
        var height = Math.round(rect.height / BLOCK_HEIGHT);

        var cluster = Factory.createCluster(rect.x, rect.y, Size(width, height), blocks[0].color);

        for (block in blocks)
        {
            board.remove(block);
            sprites.remove(block);
        }

        board.add([cluster]);
        sprites.add(cluster);

        return Some(cluster);
    }

    public static function createExpandedCluster(cluster:Block, blocks:Array<Block>, board:Board, sprites:Sprites):Option<Block>
    {
        if (!cluster.color.equals(blocks[0].color) || !allBlocksAreSingleAndSameColor(blocks))
            return None;

        var rect = getContainingRectangle(blocks);
        var cluster_rect = new Rectangle(cluster.x, cluster.y, cluster.width, cluster.height);
        var union = rect.union(cluster_rect);

        if (union.area() != (rect.area() + cluster_rect.area()))
            return None;

        var w = Math.round(union.width / BLOCK_WIDTH);
        var h = Math.round(union.height / BLOCK_HEIGHT);

        var new_cluster = Factory.createCluster(union.x, union.y, Size(w, h), cluster.color);

        blocks.push(cluster);
        for (block in blocks)
        {
            board.remove(block);
            sprites.remove(block);
        }

        board.add([new_cluster]);
        sprites.add(new_cluster);

        return Some(new_cluster);
    }

    public static function createMergedCluster(clusterL:Block, clusterR:Block, board:Board, sprites:Sprites):Option<Block>
    {
        if (!clusterL.color.equals(clusterR.color))
            return None;

        var r1 = new Rectangle(clusterL.x, clusterL.y, clusterL.width, clusterL.height);
        var r2 = new Rectangle(clusterR.x, clusterR.y, clusterR.width, clusterR.height);
        var union = r1.union(r2);

        if (union.area() != (r1.area() + r2.area()))
            return None;

        var w = Math.round(union.width / BLOCK_WIDTH);
        var h = Math.round(union.height / BLOCK_HEIGHT);

        var cluster = Factory.createCluster(union.x, union.y, Size(w, h), clusterL.color);

        board.remove(clusterL);
        sprites.remove(clusterL);
        board.remove(clusterR);
        sprites.remove(clusterR);

        board.add([cluster]);
        sprites.add(cluster);

        return Some(cluster);
    }

    static function allBlocksAreSingleAndSameColor(blocks:Array<Block>):Bool
    {
        return blocks.allOf((block) -> block.type.equals(Single)) && blocks.allOf((block) -> block.color.equals(blocks[0].color));
    }

    static function getContainingRectangle(blocks:Array<Block>):Rectangle
    {
        var min_x = blocks.findMaxElement((block, blockMax) -> return block.x < blockMax.x).x;
        var max_x = blocks.findMaxElement((block, blockMax) -> return block.x > blockMax.x).x;
        var min_y = blocks.findMaxElement((block, blockMax) -> return block.y < blockMax.y).y;
        var max_y = blocks.findMaxElement((block, blockMax) -> return block.y > blockMax.y).y;

        var width = max_x - min_x + BLOCK_WIDTH;
        var height = max_y - min_y + BLOCK_HEIGHT;

        return new Rectangle(min_x, min_y, width, height);
    }
}