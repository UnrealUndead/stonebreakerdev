package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.input.keyboard.FlxKey;

import networking.Network;
import networking.sessions.Session;
import networking.utils.NetworkEvent;
import networking.utils.NetworkMode;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.Lib;
import openfl.events.MouseEvent;

class NetworkTestState extends FlxState
{
    var is_init:Bool = false;

    override public function create():Void
    {
        super.create();

        FlxG.camera.bgColor = 0xff333399;
        FlxG.fixedTimestep = true;

    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);

        if (is_init)
            return;

        if (InputController.isPressed(FlxKey.S))
        {
            runServer();
            runClient();
            is_init = true;
        }
        else if (InputController.isPressed(FlxKey.C))
        {
            runClient();
            is_init = true;
        }
    }

    private function runServer()
    {
        var server = Network.registerSession(NetworkMode.SERVER,
            { ip: '127.0.0.1', port: 8888, max_connections: 1 });

        server.addEventListener(NetworkEvent.INIT_SUCCESS, function(event: NetworkEvent)
            {
                FlxG.camera.bgColor = 0xff339933;
                trace("INIT_SUCCESS");
            });

        server.addEventListener(NetworkEvent.INIT_FAILURE, function(event: NetworkEvent)
            {
                FlxG.camera.bgColor = 0xff993333;
                trace("INIT_FAILURE");
            });

        server.addEventListener(NetworkEvent.CONNECTED, function(event: NetworkEvent)
            {
                trace("CONNECTED");
                event.client.send({ verb:"textCommand", myData: "mmm" });
            });

        server.start();
    }

    private function runClient()
    {
        var client = Network.registerSession(NetworkMode.CLIENT,
            { ip: '127.0.0.1', port: 8888 });

            client.addEventListener(NetworkEvent.INIT_SUCCESS, function(event: NetworkEvent)
            {
                FlxG.camera.bgColor = 0xff339933;
                trace("INIT_SUCCESS");
            });

            client.addEventListener(NetworkEvent.INIT_FAILURE, function(event: NetworkEvent)
            {
                FlxG.camera.bgColor = 0xff993333;
                trace("INIT_FAILURE");
            });

        client.addEventListener(NetworkEvent.MESSAGE_RECEIVED, function(event: NetworkEvent)
            {
                switch (event.verb)
                {
                    case "textCommand":
                        trace(event.data.myData);
                    case _:
                }
            });

        client.start();
    }
}