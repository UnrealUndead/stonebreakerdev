package;

import haxe.ds.Option;
import BasicTypes;
import flixel.FlxSprite;
import BlockType;
import Globals.*;

using extensions.BasicTypesExtensions;
using extensions.OptionExtensions;

class Block
{
    public var x(get, set):Float;
    public var y(get, set):Float;
    public var width(get, never):Float;
    public var height(get, never):Float;
    public var velocity(get, set):Float;

    public var type(default, null):BlockType;
    public var color(default, null):BlockColor;
    public var state(default, null):BlockState;

    public var sprite(default, null):FlxSprite;
    public var overlay_sprite(default, null):Option<FlxSprite>;

    public function new(x:Float, y:Float, type:BlockType, color:BlockColor)
    {
        this.type = type;
        this.color = color;
        this.state = BlockState.Custom;

        switch (type)
        {
            case Single, Breaker:
                this.sprite = new BlockSprite(x, y, type, color);
                this.overlay_sprite = None;
            case ClusterBlock(s), Strike(s, _):
                var cluster_sprite = new ClusterSprite();
                cluster_sprite.updateGraphic(x, y, s.width() * BLOCK_WIDTH, s.height() * BLOCK_HEIGHT);
                cluster_sprite.loadGraphicBasedOnBlock(type, color);
                this.sprite = cluster_sprite;
                this.overlay_sprite = None;
            case Sprinkle(_):
                this.sprite = new BlockSprite(x, y, Single, color);
                this.overlay_sprite = Some(new SprinkleSprite(x, y, type));
        }
    }

    public function setState(state:BlockState)
    {
        this.state = state;

        switch(state)
        {
            case Static, Breaking, Broken, Custom:
                velocity = 0;
            case Falling:
                velocity = FALLING_VELOCITY;
        }
    }

    public function decrementTurns():Int
    {
        switch (type)
        {
            case Sprinkle(turns):
                var turns_left = turns - 1;
                if (turns_left <= 0)
                {
                    this.type = Single;
                    this.overlay_sprite = None;
                }
                else
                    this.type = Sprinkle(turns_left);
                return turns_left;

            case Strike(var size, turns):
                var turns_left = turns - 1;
                this.type = Strike(size, turns_left);
                return turns_left;

            case Single, Breaker, ClusterBlock(_):
        }
        return 0;
    }

    public function isCluster():Bool
    {
        switch (type)
        {
            case ClusterBlock(_):
                return true;
            case Single, Breaker, Sprinkle(_), Strike(_):
                return false;
        }
    }

    public function getClusterSize():Size
    {
        switch (type)
        {
            case Single, Breaker, Sprinkle(_):
                return Invalid;
            case ClusterBlock(var size), Strike(var size, _):
                return size;
        }
    }

    inline function get_x()
        return sprite.x;
    function set_x(value:Float):Float
    {
        switch (overlay_sprite)
        {
            case Some(s):
                s.x = value;
            case None:
        }
        return sprite.x = value;
    }
    inline function get_y()
        return sprite.y;
    function set_y(value:Float):Float
    {
        switch (overlay_sprite)
        {
            case Some(s):
                s.y = value;
            case None:
        }
        return sprite.y = value;
    }
    inline function get_width()
        return sprite.width;
    inline function get_height()
        return sprite.height;
    inline function get_velocity()
        return sprite.velocity.y;
    function set_velocity(value:Float):Float
    {
        switch (overlay_sprite)
        {
            case Some(s):
                s.velocity.y = value;
            case None:
        }
        return sprite.velocity.y = value;
    }
}