package;

import BlockType.BlockColor;
import InputState;
import flixel.util.FlxSort;
import flixel.FlxSprite;
import BasicTypes;
import flixel.input.keyboard.FlxKey;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.FlxState;
import haxe.ds.Option;
import BlockPair.RotationDirection;
import Globals.*;

using extensions.OptionExtensions;

class TestState extends FlxState
{
    var input_controller:InputController;
    var player_blocks:BlockPair;
    var play_block:Block;
    var board:Board;
    var clusters:Set<Block>;
    var sprites:Sprites;

    override public function create():Void
    {
        super.create();

        FlxG.camera.bgColor = 0xff333333;
        FlxG.fixedTimestep = true;

        input_controller = new InputController();
        input_controller.subscribeToKeyEvent(FlxKey.SPACE, new Range<Float>(0.5, 0, 0.5));

        sprites = new Sprites();
        add(sprites.group);
        clusters = new Set<Block>();

        board = new Board(0,320,sprites);

        Factory.addNewBlock(Position(0,0), Single, Red, board, sprites);
        Factory.addNewBlock(Position(1,0), Single, Red, board, sprites);
    }

    function addPlayerBlocks():Void
    {
        var pivot_block = new Block(0, BLOCK_HEIGHT, Single, Red);
        add(pivot_block.sprite);
        var rotating_block = new Block(0, 0, Single, Yellow);
        add(rotating_block.sprite);
        player_blocks = new BlockPair(pivot_block, rotating_block);
        add(player_blocks.selection_sprite);
    }

    function addPlayBlock(x:Float, y:Float):Void
    {
        play_block = new Block(x, y, Single, Red);
        add(play_block.sprite);
    }

    override public function update(elapsed:Float):Void
    {
        if (InputController.isPressed(FlxKey.F5))
        {
            FlxG.resetGame();
            return;
        }

        if (player_blocks == null)
            addPlayerBlocks();

        //if (play_block == null)
        //    addPlayBlock(96, 312);

        var collision = Collision.getPlayerCollision(player_blocks, board);

        if (InputController.isPressed(FlxKey.UP) && !collision.has(GeneralDirection.Up))
        {
            player_blocks.y -= 8;
        }
        if (InputController.isPressed(FlxKey.DOWN) && !collision.has(GeneralDirection.Down))
        {
            player_blocks.y += 8;
        }

        collision = Collision.getPlayerCollision(player_blocks, board);

        if (InputController.isPressed(FlxKey.LEFT) && !collision.has(GeneralDirection.Left))
        {
            player_blocks.x -= BLOCK_WIDTH;
        }
        if (InputController.isPressed(FlxKey.RIGHT) && !collision.has(GeneralDirection.Right))
        {
            player_blocks.x += BLOCK_WIDTH;
        }

        if (InputController.isPressed(FlxKey.X))
        {
            MovementController.rotateBlockPair(player_blocks, board, RotationDirection.CLOCKWISE, elapsed);
        }
        else if (InputController.isPressed(FlxKey.C))
        {
            MovementController.rotateBlockPair(player_blocks, board, RotationDirection.COUNTER_CLOCKWISE, elapsed);
        }

        /*if (input_controller.isKeyPressed(FlxKey.SPACE, elapsed))
        {
            var play_block = this.play_block;
            this.play_block = null;
            board.add([play_block]);
            if (play_block.type.equals(Breaker))
                return;
        }*/

        super.update(elapsed);
    }
}
