package;

import flixel.FlxSprite;
import flixel.group.FlxGroup.FlxTypedGroup;
import Globals.*;

class Sprites
{
    static var SPRITES_PER_BLOCK = 2;
    public var group(default, null):FlxTypedGroup<FlxSprite>;

    public function new()
    {
        group = new FlxTypedGroup<FlxSprite>(2 * SPRITES_PER_BLOCK * BOARD_X_CAPACITY * BOARD_Y_CAPACITY);
    }

    public function add(?block:Block, ?blockPair:BlockPair)
    {
        if (group.countLiving() == group.maxSize)
            trace("WARNING: More sprites added then group allows");

        if (block != null)
        {
            group.add(block.sprite);
            switch (block.overlay_sprite)
            {
                case Some(overlay):
                    group.add(overlay);
                case None:
            }
        }
        if (blockPair != null)
        {
            group.add(blockPair.pivot_block.sprite);
            group.add(blockPair.selection_sprite);
            group.add(blockPair.rotating_block.sprite);
        }
    }

    public function remove(?block:Block, ?blockPair:BlockPair)
    {
        if (block != null)
        {
            switch (block.overlay_sprite)
            {
                case Some(overlay):
                    group.remove(overlay, true);
                case None:
                    group.remove(block.sprite, true);
            }
        }
        if (blockPair != null)
        {
            group.remove(blockPair.pivot_block.sprite, true);
            group.remove(blockPair.selection_sprite, true);
            group.remove(blockPair.rotating_block.sprite, true);
        }
    }
}