package;

import haxe.ds.ObjectMap;

@:forward(clear, remove)
abstract Set<T:{}>(ObjectMap<T, Bool>) from ObjectMap<T, Bool> to ObjectMap<T, Bool>
{
    public var length(get, never) : Int;

    public function new(?it:Iterable<T>)
    {
        this = new ObjectMap<T, Bool>();
        if (it != null)
            for(value in it)
                add(value);
    }

    public function add(v:T):Void
    {
        if (v == null) // key of null is undefined behaviour
            return;
        this.set(v, true);
    }

    public inline function copy():Set<T>
    {
        return this.copy();
    }

    public inline function contains(v:T):Bool
    {
        return this.exists(v);
    }

    public function empty():Bool
    {
        for(v in this)
            return false;
        return true;
    }

    public inline function iterator()
    {
        return this.keys();
    }

    @:op(A+B)
    public function concat(set:Set<T>):Set<T>
    {
        var result:Set<T> = this.copy();
        for(value in set)
            result.add(value);
        return result;
    }

    @:to
    public function toArray() : Array<T>
    {
        return [for(key in this.keys()) key];
    }

    @:to
    public function toString()
    {
        return "{" + toArray().join(", ") + "}";
    }

    function get_length()
    {
        var length = 0;
        for(i in this)
            length++;
        return length;
    }
}