package;

import haxe.ds.Option;
import BlockType.BlockColor;

using extensions.ArrayExtensions;
using extensions.OptionExtensions;

class BreakingBlocks
{
    var breaking_blocks_queue:Queue<Set<Block>>;

    public function new()
    {
        breaking_blocks_queue = new Queue<Set<Block>>();
    }

    public function updateBreakers(board:Board)
    {
        for (breaker in board.getBreakers())
        {
            var adjacent_breakable_blocks = getAdjacentBreakableBlocks(breaker, board, new Set<Block>());
            if (!adjacent_breakable_blocks.empty())
            {
                add(breaker);
            }
        }

        if (!breaking_blocks_queue.empty())
        {
            propagateBreaking(board);
            mergeFirstTwoArrays();
        }
    }

    public function dequeue():Option<Set<Block>>
    {
        if (breaking_blocks_queue.empty())
            return None;
        return Some(breaking_blocks_queue.pop());
    }

    public function empty():Bool
    {
        return breaking_blocks_queue.empty();
    }

    function add(block:Block, depth:Int = 0)
    {
        if (breaking_blocks_queue.length > depth)
        {
            block.setState(Breaking);
            breaking_blocks_queue[depth].add(block);
        }
        else if (breaking_blocks_queue.length == depth)
        {
            block.setState(Breaking);
            breaking_blocks_queue.push(new Set<Block>([block]));
        }
    }

    function getAdjacentBreakableBlocks(block:Block, board:Board, exclude:Set<Block>):Set<Block>
    {
        var adjacent_blocks = new Set<Block>();

        var adjacent = board.getAdjacentBlocks(block).getDirectlyAdjacent();
        for (adjacent_block in adjacent)
        {
            var is_correct_color = adjacent_block.color.equals(block.color);
            var is_correct_state = adjacent_block.state.equals(Static);

            var is_correct_type = false;
            switch (adjacent_block.type)
            {
                case Single, Breaker, ClusterBlock(_):
                    is_correct_type = true;
                case Sprinkle(_), Strike(_):
            }

            if (is_correct_color && is_correct_state && is_correct_type && !exclude.contains(adjacent_block))
                adjacent_blocks.add(adjacent_block);
        }

        return adjacent_blocks;
    }

    function propagateBreaking(board:Board, depth:Int = 1)
    {
        var next_breaking = new Set<Block>();
        var breaking_at_previous_depth = breaking_blocks_queue.empty() ? new Set<Block>() : breaking_blocks_queue[depth - 1];
        for (previous_breaking in breaking_at_previous_depth)
        {
            var breakable_adjacent = getAdjacentBreakableBlocks(previous_breaking, board, next_breaking);
            for (adjacent in breakable_adjacent)
                next_breaking.add(adjacent);
        }

        if (!next_breaking.empty())
        {
            for (breakable in next_breaking)
                add(breakable, depth);

            propagateBreaking(board, depth + 1);
        }
    }

    function mergeFirstTwoArrays()
    {
        if (breaking_blocks_queue.length >= 2)
        {
            breaking_blocks_queue.pushFront(breaking_blocks_queue.pop() + breaking_blocks_queue.pop());
        }
    }
}