package;

@:generic
class Range<T>
{
    public var min:T;
    public var delta:T;
    public var max:T;

    public function new(min:T, delta:T, max:T) 
    {
        this.min = min;
        this.delta = delta;
        this.max = max;
    }
}

class InputState
{
    public var elapsed(default, default):Float;
    public var timeout(default, default):Float;

    public var timings(get, null):Range<Float>;

    public function new(timings:Range<Float>)
    {
        this.timings = timings;

        elapsed = 0;
        timeout = timings.max;
    }

    function get_timings():Range<Float>
    {
        return new Range<Float>(timings.min, timings.delta, timings.max);
    }
}