package;

enum Position
{
    Invalid;
    Position(x:Int, y:Int);
}

enum Size
{
    Invalid;
    Size(width:Int, height:Int);
}

enum GeneralDirection
{
    Invalid;
    Up;
    Down;
    Left;
    Right;
}

enum Direction
{
    Invalid;
    Up(value:Float);
    Down(value:Float);
    Left(value:Float);
    Right(value:Float);
}

//typedef Position = { x:Int, y:Int }

//typedef Size = { width:Int, height:Int }

//typedef Rectangle = Position & Size;