package;

class CachedValue<T>
{
    var dirty:Bool;
    var value:T;
    var updater:Void->T;

    public function new(updater:Void->T)
    {
        this.updater = updater;
        dirty = true;
    }

    public inline function markDirty()
    {
        dirty = true;
    }

    public function get():T
    {
        if (dirty)
        {
            value = updater();
            dirty = false;
        }

        return value;
    }
}