package;

import BasicTypes.Size;
import BlockType;

using extensions.BasicTypesExtensions;

class AttackCalculator
{
    var attack:AttackGroup;
    var combo_modifier:Int;

    public function new()
    {
        reset();
    }

    public function add(blocks:Set<Block>)
    {
        updateSprinkles(blocks);
        updateStrikes(blocks);

        combo_modifier++;
    }

    public function takeAttack():AttackGroup
    {
        var result = attack;
        reset();
        return result;
    }

    public function empty():Bool
    {
        return attack.sprinkles == 0 && attack.strikes.empty();
    }

    function updateSprinkles(blocks:Set<Block>)
    {
        var sprinkle_count = 0;
        for (block in blocks)
        {
            switch (block.type)
            {
                case Single, Breaker:
                    sprinkle_count += 1;
                case _:
            }
        }
        attack.sprinkles += Math.floor((sprinkle_count * combo_modifier) * 0.5);
    }

    function updateStrikes(blocks:Set<Block>)
    {
        for (block in blocks)
        {
            switch (block.type)
            {
                case ClusterBlock(var size):
                    attack.strikes.push(Size(size.width(), size.height() * combo_modifier));
                case _:
            }
        }
    }

    function reset()
    {
        attack = new AttackGroup(0, new Queue<Size>());
        combo_modifier = 1;
    }
}