package;

@:forward(length, push)
abstract Queue<T>(Array<T>)
{
    public inline function new(?it:Iterable<T>)
    {
        this = new Array<T>();
        if (it != null)
            for(value in it)
                this.push(value);
    }

    public inline function first():T
    {
        return this[0];
    }

    public inline function last():T
    {
        return this[this.length - 1];
    }

    public function empty():Bool
    {
        return this.length == 0;
    }

    public inline function pushFront(value:T)
    {
        this.unshift(value);
    }

    public inline function pop():T
    {
        return this.shift();
    }

    @:arrayAccess
    public inline function peek(index:Int):T
    {
        return this[index];
    }

    public inline function iterator()
    {
        return this.iterator();
    }
}