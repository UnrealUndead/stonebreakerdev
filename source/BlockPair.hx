package;

enum RotatingState
{
    UP;
    RIGHT;
    DOWN;
    LEFT;
}

enum RotationDirection
{
    CLOCKWISE;
    COUNTER_CLOCKWISE;
}

class BlockPair
{
    public var pivot_block:Block;//TODO remove public
    public var rotating_block:Block;//TODO remove public
    public var state:RotatingState;//TODO remove public
    public var selection_sprite:OutlineSprite;

    public var x(get, set):Float;
    public var y(get, set):Float;
    public var velocity(get, set):Float;
    public var width(get, null):Float;
    public var height(get, null):Float;


    public function new(firstBlock:Block, secondBlock:Block)
    {
        pivot_block = firstBlock;
        rotating_block = secondBlock;
        state = RotatingState.UP;
        selection_sprite = new OutlineSprite(pivot_block.x, pivot_block.y, pivot_block.type.equals(Breaker));
    }

    function get_x():Float
    {
        return pivot_block.x;
    }

    function set_x(x:Float):Float
    {
        if (state == RotatingState.LEFT)
        {
            pivot_block.x = x;
            rotating_block.x = x - pivot_block.width;
        }
        else if (state == RotatingState.RIGHT)
        {
            pivot_block.x = x;
            rotating_block.x = x + pivot_block.width;
        }
        else
        {
            pivot_block.x = x;
            rotating_block.x = x;
        }

        selection_sprite.x = pivot_block.x;

        return get_x();
    }

    public function isVerticallyOriented()
    {
        return state == RotatingState.UP || state == RotatingState.DOWN;
    }

    function get_y():Float
    {
        return pivot_block.y;
    }

    function set_y(y:Float):Float
    {
        if (state == RotatingState.UP)
        {
            pivot_block.y = y;
            rotating_block.y = pivot_block.y - pivot_block.height;
        }
        else if (state == RotatingState.DOWN)
        {
            pivot_block.y = y;
            rotating_block.y = pivot_block.y + pivot_block.height;
        }
        else
        {
            pivot_block.y = y;
            rotating_block.y = y;
        }

        selection_sprite.y = pivot_block.y;

        return get_y();
    }

    function get_velocity():Float
    {
        return Math.max(pivot_block.velocity, rotating_block.velocity);
    }

    function set_velocity(velocity:Float):Float
    {
        pivot_block.velocity = velocity;
        rotating_block.velocity = velocity;
        selection_sprite.velocity.y = pivot_block.velocity;

        return get_velocity();
    }

    function get_width():Float
    {
        if (isVerticallyOriented())
        {
            return Math.max(pivot_block.width, rotating_block.width);
        }
        else
        {
            return pivot_block.width + rotating_block.width;
        }
    }

    function get_height():Float
    {
        if (isVerticallyOriented())
        {
            return pivot_block.height + rotating_block.height;
        }
        else
        {
            return Math.max(pivot_block.height, rotating_block.height);
        }
    }

    public static function getNextRotationState(currentState:RotatingState, direction:RotationDirection):RotatingState
    {
        switch (direction)
        {
            case RotationDirection.CLOCKWISE:
                switch (currentState)
                {
                    case RotatingState.UP:
                        return RotatingState.RIGHT;
                    case RotatingState.RIGHT:
                        return RotatingState.DOWN;
                    case RotatingState.DOWN:
                        return RotatingState.LEFT;
                    case RotatingState.LEFT:
                        return RotatingState.UP;
                }
            case RotationDirection.COUNTER_CLOCKWISE:
                switch (currentState)
                {
                    case RotatingState.UP:
                        return RotatingState.LEFT;
                    case RotatingState.LEFT:
                        return RotatingState.DOWN;
                    case RotatingState.DOWN:
                        return RotatingState.RIGHT;
                    case RotatingState.RIGHT:
                        return RotatingState.UP;
                }
        }
    }

    public function rotate(direction:RotationDirection):Void
    {
        switch (direction)
        {
            case RotationDirection.CLOCKWISE:
                switch (state)
                {
                    case RotatingState.UP:
                        rotating_block.x = pivot_block.x + pivot_block.width;
                        rotating_block.y = pivot_block.y;
                    case RotatingState.RIGHT:
                        rotating_block.x = pivot_block.x;
                        rotating_block.y = pivot_block.y + pivot_block.height;
                    case RotatingState.DOWN:
                        rotating_block.y = pivot_block.y;
                        rotating_block.x = pivot_block.x - pivot_block.width;
                    case RotatingState.LEFT:
                        rotating_block.x = pivot_block.x;
                        rotating_block.y = pivot_block.y - pivot_block.height;
                }
            case RotationDirection.COUNTER_CLOCKWISE:
                switch (state)
                {
                    case RotatingState.UP:
                        rotating_block.x = pivot_block.x - pivot_block.width;
                        rotating_block.y = pivot_block.y;
                    case RotatingState.LEFT:
                        rotating_block.x = pivot_block.x;
                        rotating_block.y = pivot_block.y + pivot_block.height;
                    case RotatingState.DOWN:
                        rotating_block.y = pivot_block.y;
                        rotating_block.x = pivot_block.x + pivot_block.width;
                    case RotatingState.RIGHT:
                        rotating_block.x = pivot_block.x;
                        rotating_block.y = pivot_block.y - pivot_block.height;
                }
        }

        state = getNextRotationState(state, direction);
    }
}