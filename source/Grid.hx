package;

import lime.math.Rectangle;
import BasicTypes;
import Globals.*;

using extensions.BasicTypesExtensions;
using extensions.RectangleExtensions;

class Grid
{
    public var size(get, never):Size;

    var cell_size:Size = Invalid;
    var grid:Rectangle;

    public function new(position:Position, cellSize:Size, cellAmount:Size)
    {
        cell_size = cellSize;

        var grid_width = cellAmount.width() * cellSize.width();
        var grid_height = cellAmount.height() * cellSize.height();
        grid = new Rectangle(position.x(), position.y(), grid_width, grid_height);
    }

    public function containsPartially(rect:Rectangular)
    {
        return grid.intersects(rect);
    }

    public function getPosition(rect:Rectangular, bySnap:Bool = false):Position
    {
        var rectangle = rect.toRectangle();

        if (bySnap)
            snapToGrid(rectangle, true);

        if (!grid.containsRect(rectangle))
        {
            trace('WARNING: Invalid grid position');
            return Invalid;
        }

        var x = Math.round((rectangle.x - grid.x) / cell_size.width());
        var y = Math.round((rectangle.y - grid.y) / cell_size.height());

        return Position(x, y);
    }

    public function snapToGrid(rect:Rectangular, silent:Bool = false)
    {
        var rectangle = rect.toRectangle();

        if (!grid.containsRect(rectangle) && !silent)
            trace('WARNING: Position is outside of grid for rect: R(x:${rect.x}, y:${rect.y}, w:${rect.width}, h:${rect.height})');

        rectangle.left = Math.max(rectangle.left, grid.left);
        rectangle.right = Math.min(rectangle.right, grid.right);
        rect.x = getClosestNumber(rectangle.x - grid.x, cell_size.width()) + grid.x;

        rectangle.top = Math.max(rectangle.top, grid.top);
        rectangle.bottom = Math.min(rectangle.bottom, grid.bottom);
        rect.y = getClosestNumber(rectangle.y - grid.y, cell_size.height()) + grid.y;
    }

    function get_size():Size
    {
        return Size(Std.int(grid.width / cell_size.width()), Std.int(grid.height / cell_size.height()));
    }

    static function getClosestNumber(number:Float, divisible:Float):Int
    {
        var quotient = Math.round(number / divisible);
        var candidate_1 = divisible * quotient;

        var candidate_2;
        if (number * divisible > 0)
        {
            candidate_2 = divisible * (quotient + 1);
        }
        else
        {
            candidate_2 = divisible * (quotient - 1);
        }

        if (Math.abs(number - candidate_1) < Math.abs(number - candidate_2))
        {
            return Math.round(candidate_1);
        }
        return Math.round(candidate_2);
    }
}