package;

import BlockType.BlockColor;
import flixel.math.FlxRandom;
import flixel.FlxSprite;
import Globals.*;

class BlockSprite extends FlxSprite
{
    public function new(x:Float, y:Float, type:BlockType, color:BlockColor)
    {
        super(x, y);
        setSize(BLOCK_WIDTH, BLOCK_HEIGHT);

        loadGraphicBasedOnType(type, color);

        //origin.y = BLOCK_WIDTH;
        //offset.x = BLOCK_WIDTH / 2;
        //angle = 90;
        //immovable = true;
        ////solid = true;

        //trace(getHitbox());
    }

    public function loadGraphicBasedOnType(type:BlockType, color:BlockColor)
    {
        switch (type)
        {
            case Single, Sprinkle(_):
                alpha = 1;
                switch (color)
                {
                    case Red:
                        loadGraphic(AssetPaths.element_red_rectangle__png, false, BLOCK_HEIGHT, BLOCK_WIDTH);
                    case Blue:
                        loadGraphic(AssetPaths.element_blue_rectangle__png, false, BLOCK_HEIGHT, BLOCK_WIDTH);
                    case Green:
                        loadGraphic(AssetPaths.element_green_rectangle__png, false, BLOCK_HEIGHT, BLOCK_WIDTH);
                    case Yellow:
                        loadGraphic(AssetPaths.element_yellow_rectangle__png, false, BLOCK_HEIGHT, BLOCK_WIDTH);
                }
            case Breaker:
                alpha = 1;
                switch (color)
                {
                    case Red:
                        loadGraphic(AssetPaths.red_breaker__png, false, BLOCK_HEIGHT, BLOCK_WIDTH);
                    case Blue:
                        loadGraphic(AssetPaths.blue_breaker__png, false, BLOCK_HEIGHT, BLOCK_WIDTH);
                    case Green:
                        loadGraphic(AssetPaths.green_breaker__png, false, BLOCK_HEIGHT, BLOCK_WIDTH);
                    case Yellow:
                        loadGraphic(AssetPaths.yellow_breaker__png, false, BLOCK_HEIGHT, BLOCK_WIDTH);
                }
            case ClusterBlock(_), Strike(_):
        }
    }
}