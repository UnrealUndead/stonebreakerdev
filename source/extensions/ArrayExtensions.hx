package extensions;

import haxe.ds.Option;

using extensions.OptionExtensions;

class ArrayExtensions
{
    /**
        Finds first element that satifies the predicate.
    **/
    public static function find<T>(elements:Array<T>, predicate:T->Bool):Option<T>
    {
        for (element in elements)
        {
            if (predicate(element))
            {
                return Some(element);
            }
        }
        return None;
    }

    public static function contains<T>(elements:Array<T>, element:T):Bool
    {
        return find(elements, (array_element) -> array_element == element).exists(); // TODO find all places that use == for comparing enums
    }

    /**
        Compares all elements against each other.
        Example usage is for min/max element.
    **/
    public static function findMaxElement<T>(elements:Array<T>, compareFunc:(T,T)->Bool):T
    {
        var most_fit_element = elements[0];
        for (element in elements)
        {
            if (compareFunc(element, most_fit_element))
            {
                most_fit_element = element;
            }
        }
        return most_fit_element;
    }

    public static function empty<T>(elements:Array<T>):Bool
    {
        return elements.length == 0;
    }

    public static function first<T>(elements:Array<T>):T
    {
        return elements[0];
    }

    public static function last<T>(elements:Array<T>):T
    {
        return elements[elements.length - 1];
    }

    public static function anyOf<T>(elements:Array<T>, predicate:(T)->Bool):Bool
    {
        for (element in elements)
        {
            if (predicate(element))
                return true;
        }
        return false;
    }

    public static function allOf<T>(elements:Array<T>, predicate:(T)->Bool):Bool
    {
        for (element in elements)
        {
            if (!predicate(element))
                return false;
        }
        return true;
    }

    public static function copyExisting<T>(elements:Array<Option<T>>):Array<T>
    {
        var result = [];
        for (element in elements)
        {
            switch (element)
            {
                case Some(value):
                    result.push(value);
                case _:
            }
        }
        return result;
    }

    public static function popFirst<T>(elements:Array<T>):T
    {
        return elements.shift();
    }

    public static function filterMap<T, U>(elements:Array<T>, mapFunc:(T)->Option<U>):Array<U>
    {
        var result = new Array<U>();
        for (element in elements)
        {
            switch (mapFunc(element))
            {
                case Some(value):
                    result.push(value);
                case None:
            }
        }
        return result;
    }

    public static function rotate<T>(elements:Array<T>, amount:Int):Void
    {
        if (amount >= 0)
            for (i in 0...amount)
                elements.push(elements.shift());
        else
            for (i in 0...amount)
                elements.unshift(elements.pop());
    }
}