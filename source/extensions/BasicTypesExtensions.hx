package extensions;

import BasicTypes;

class BasicTypesExtensions
{
    public static function width(size:Size):Int
    {
        switch (size)
        {
            case Size(width, _):
                return width;
            case Invalid:
                return -1;
        }
    }

    public static function height(size:Size):Int
    {
        switch (size)
        {
            case Size(_, height):
                return height;
            case Invalid:
                return -1;
        }
    }

    public static function add(size:Size, size2:Size):Size
    {
        switch (size)
        {
            case Size(width, height):
                switch (size2)
                {
                    case Size(width2, height2):
                        return Size(width + width2, height + height2);
                    case _:
                }
            case _:
        }
        return Invalid;
    }

    public static function x(pos:Position):Int
    {
        switch (pos)
        {
            case Position(x, _):
                return x;
            case Invalid:
                return 0;
        }
    }

    public static function y(pos:Position):Int
    {
        switch (pos)
        {
            case Position(_, y):
                return y;
            case Invalid:
                return 0;
        }
    }
}