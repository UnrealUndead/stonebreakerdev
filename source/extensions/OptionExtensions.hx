package extensions;

import haxe.ds.Option;

class OptionExtension
{
    public static function value<T>(optional:Option<T>):Null<T>
    {
        switch (optional)
        {
            case Some(value):
                return value;
            case None:
                return null;
        }
    }

    public static function valueOr<T>(optional:Option<T>, other:T):T
    {
        switch (optional)
        {
            case Some(value):
                return value;
            case None:
                return other;
        }
    }

    public static function exists<T>(optional:Option<T>):Bool
    {
        switch (optional)
        {
            case Some(_):
                return true;
            case None:
                return false;
        }
    }
}