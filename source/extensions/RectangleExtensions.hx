package extensions;

import lime.math.Rectangle;

class RectangleExtensions
{
    public static function extend(rectangle:Rectangle, up:Int, left:Int, right:Int, down:Int)
    {
        rectangle.y -= up;
        rectangle.height += up + down;
        rectangle.x -= left;
        rectangle.width += left + right;
    }

    public static function area(rectangle:Rectangle):Float
    {
        return rectangle.width * rectangle.height;
    }
}