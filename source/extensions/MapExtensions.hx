package extensions;

class MapExtensions
{
    public static function values<K, V>(map:Map<K, V>):Array<V>
    {
        var array = new Array<V>();
        for (value in map)
        {
            array.push(value);
        }
        return array;
    }
}