package extensions;

import haxe.EnumFlags;

class EnumFlagsExtensions
{
    public static function isEmpty<T:EnumValue>(flags:EnumFlags<T>):Bool
    {
        return flags.toInt() == 0;
    }
}