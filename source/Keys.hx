package;

import flixel.input.keyboard.FlxKey;

class Keys
{
    public static var MOVE_LEFT = FlxKey.LEFT;
    public static var MOVE_RIGHT = FlxKey.RIGHT;
    public static var MOVE_DOWN = FlxKey.SPACE;
    public static var ROTATE_CLOCKWISE = FlxKey.DOWN;
    public static var ROTATE_COUNTER_CLOCKWISE = FlxKey.UP;
}