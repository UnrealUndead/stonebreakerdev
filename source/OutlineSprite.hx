package;

import flixel.FlxSprite;
import Globals.*;

class OutlineSprite extends FlxSprite
{
    public function new(x:Float, y:Float, isBreaker:Bool)
    {
        super(x, y);
        switch (isBreaker)
        {
            case false:
                loadGraphic(AssetPaths.outline_rectangle__png, false, BLOCK_WIDTH, BLOCK_HEIGHT);
            case true:
                loadGraphic(AssetPaths.outline_breaker__png, false, BLOCK_WIDTH, BLOCK_HEIGHT);
        }
    }
}