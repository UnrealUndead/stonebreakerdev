package;

import BlockType.BlockColor;
import flixel.math.FlxRandom;
import flixel.FlxSprite;
import Globals.*;

class SprinkleSprite extends FlxSprite
{
    public function new(x:Float, y:Float, type:BlockType)
    {
        super(x, y);
        setSize(BLOCK_WIDTH, BLOCK_HEIGHT);

        loadGraphicBasedOnType(type);
    }

    public function loadGraphicBasedOnType(type:BlockType)
    {
        switch (type)
        {
            case Sprinkle(turns):
                alpha = turns > 1 ? 1 : SPRINKLES_OPACITY;
                loadGraphic(AssetPaths.element_grey_rectangle__png, false, BLOCK_HEIGHT, BLOCK_WIDTH);
            case Single, Breaker, ClusterBlock(_), Strike(_):
        }
    }
}