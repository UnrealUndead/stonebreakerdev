package;

import AttackGroup;
import BasicTypes;
import Globals.*;
import haxe.ds.Option;

using extensions.OptionExtensions;

class AttackReceiver
{
    var board:Board;
    var sprites:Sprites;
    var queued_attacks:Queue<AttackGroup>;
    var incoming_attack:Option<AttackGroup>;
    var current_attack:Option<AttackGroup>;
    var attack_left_size:Bool;
    var strike_position:Int;

    public function new(board:Board, sprites:Sprites)
    {
        this.board = board;
        this.sprites = sprites;
        this.queued_attacks = new Queue<AttackGroup>();
        this.incoming_attack = None;
        this.current_attack = None;
        this.attack_left_size = false;
        this.strike_position = 2;
    }

    public function addAttack(attack:AttackGroup)
    {
        queued_attacks.push(attack);
    }

    public function prepare()
    {
        if (current_attack.exists())
        {
            trace("ERROR: Current attack still exists!");
            return;
        }

        current_attack = incoming_attack;

        if (queued_attacks.empty())
        {
            incoming_attack = None;
            return;
        }

        incoming_attack = Some(queued_attacks.pop());
    }

    public function updateAttacks()
    {
        if (!current_attack.exists())
        {
            return;
        }

        var attack = current_attack.value();
        if (!attack.strikes.empty())
        {
            AttackFactory.createStrikes(attack.strikes, attack_left_size, strike_position, board, sprites);
            attack.strikes = new Queue<Size>();
            strike_position = (strike_position % BOARD_X_CAPACITY) + 1;
            return;
        }

        if (attack.sprinkles > 0)
        {
            AttackFactory.createSprinkles(attack.sprinkles, attack_left_size, board, sprites);
            attack.sprinkles = 0;
        }

        current_attack = None;
        attack_left_size = !attack_left_size;
    }

    public function hasIncomingAttack():Bool
    {
        return incoming_attack.exists();
    }

    public function hasAttackForThisTurn():Bool
    {
        return current_attack.exists();
    }
}